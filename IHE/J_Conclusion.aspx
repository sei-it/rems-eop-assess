﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="J_Conclusion.aspx.cs" Inherits="J_Conclusion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
     
<h1>Conclusion</h1>


 <fieldset class="assessForm">
  <p>
      Thank you for taking time to complete this self-assessment, designed to help 
      you gauge your capacity to create and maintain a high-quality emergency operations 
      plan for an institution of higher education that considers a range of threats and 
      hazards. You are now familiar with the key considerations involved and important 
      questions to consider as you engage in the process of developing a high-quality 
      plan. Upon clicking the “next” button, you will be presented with a customized 
      downloadable report containing a variety of additional resources and information, 
      based upon your responses to the assessment questions. For additional information 
      on the planning process as recommended by the U.S. Departments of Education, Homeland 
      Security, Justice, and Health and Human Services, as well as on questions and concerns 
      regarding your report, please contact the Readiness and Emergency Management for Schools
      (REMS) Technical Assistance (TA) Center Help Desk by email<a href="mailTo:info@remstacenter.org"> (info@remstacenter.org)</a> or by 
      telephone (855-781-7367 [REMS]), toll free and between the hours of 9:00 a.m. and 5:00 p.m., 
      Eastern Standard Time. This concludes the self-assessment. Click NEXT to generate your report.
  </p>  
       
</fieldset>
     <div class="prevNext">       
       <asp:Button ID="btnBack" runat="server" Visible="false" Text="Go Back" OnClick="btnBack_Click" />
       <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

