﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class H_Recovery : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }

    protected void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.AssessmentQuestionID == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.RECOVERY_A1_CON_BUSINESS_OPERATIONS_FK;
                if (objVal != null)
                {
                    rbtContinuityBussOperations.SelectedIndex = rbtContinuityBussOperations.Items.IndexOf(rbtContinuityBussOperations.Items.FindByValue(oRes.RECOVERY_A1_CON_BUSINESS_OPERATIONS_FK.ToString()));
                }
                objVal = oRes.RECOVERY_A2_CON_TEACHING_LEARNING_FK;
                if (objVal != null)
                {
                    rbtContinuityTeaching.SelectedIndex = rbtContinuityTeaching.Items.IndexOf(rbtContinuityTeaching.Items.FindByValue(oRes.RECOVERY_A2_CON_TEACHING_LEARNING_FK.ToString()));
                }
                objVal = oRes.RECOVERY_A3_HELP_STUDENTS_STAFF_FK;
                if (objVal != null)
                {
                    rbtHelpingStudents.SelectedIndex = rbtHelpingStudents.Items.IndexOf(rbtHelpingStudents.Items.FindByValue(oRes.RECOVERY_A3_HELP_STUDENTS_STAFF_FK.ToString()));
                }
                objVal = oRes.RECOVERY_B_SCHOOL_CONSIDERED_STRATEGIES_FK;
                if (objVal != null)
                {
                    rbtIHEconsiderStrategies.SelectedIndex = rbtIHEconsiderStrategies.Items.IndexOf(rbtIHEconsiderStrategies.Items.FindByValue(oRes.RECOVERY_B_SCHOOL_CONSIDERED_STRATEGIES_FK.ToString()));
                }
            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.AssessmentQuestionID == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;
            }

            if (!(rbtContinuityBussOperations.SelectedItem == null))
            {
                if (!(rbtContinuityBussOperations.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_A1_CON_BUSINESS_OPERATIONS_FK = Convert.ToInt32(rbtContinuityBussOperations.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_A1_CON_BUSINESS_OPERATIONS_FK = null;
                }
            }
            if (!(rbtContinuityTeaching.SelectedItem == null))
            {
                if (!(rbtContinuityTeaching.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_A2_CON_TEACHING_LEARNING_FK = Convert.ToInt32(rbtContinuityTeaching.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_A2_CON_TEACHING_LEARNING_FK = null;
                }
            }

            if (!(rbtHelpingStudents.SelectedItem == null))
            {
                if (!(rbtHelpingStudents.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_A3_HELP_STUDENTS_STAFF_FK = Convert.ToInt32(rbtHelpingStudents.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_A3_HELP_STUDENTS_STAFF_FK = null;
                }
            }
            if (!(rbtIHEconsiderStrategies.SelectedItem == null))
            {
                if (!(rbtIHEconsiderStrategies.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_B_SCHOOL_CONSIDERED_STRATEGIES_FK = Convert.ToInt32(rbtIHEconsiderStrategies.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_B_SCHOOL_CONSIDERED_STRATEGIES_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = oRes.AssessmentQuestionID;
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from e in db.YesNo_Nots
                      select e;

            rbtContinuityBussOperations.DataSource = qry;
            rbtContinuityBussOperations.DataTextField = "Yes_No_Not";
            rbtContinuityBussOperations.DataValueField = "NotID";
            rbtContinuityBussOperations.DataBind();

            rbtContinuityTeaching.DataSource = qry;
            rbtContinuityTeaching.DataTextField = "Yes_No_Not";
            rbtContinuityTeaching.DataValueField = "NotID";
            rbtContinuityTeaching.DataBind();


            rbtHelpingStudents.DataSource = qry;
            rbtHelpingStudents.DataTextField = "Yes_No_Not";
            rbtHelpingStudents.DataValueField = "NotID";
            rbtHelpingStudents.DataBind();


            rbtIHEconsiderStrategies.DataSource = qry;
            rbtIHEconsiderStrategies.DataTextField = "Yes_No_Not";
            rbtIHEconsiderStrategies.DataValueField = "NotID";
            rbtIHEconsiderStrategies.DataBind();


        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("I_OtherConsideration.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("G_TrainingDocumentation.aspx?lngPkID=" + lngPkID);
    }
}