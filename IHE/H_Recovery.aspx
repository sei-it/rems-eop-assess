﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="H_Recovery.aspx.cs" Inherits="H_Recovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    <script type="text/javascript">

        function showPos(event, text) {
            var el;
            el = document.getElementById('PopUp');
            // el.style.display = "block";
            $("#PopUp").bPopup();
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
     <h1>Recovery</h1>

<fieldset class="assessForm"> 
    <legend>It is important that your IHE normalize operations as quickly as possible
       following an emergency event, in order to focus on the physical and emotional well-being of students, faculty, staff,
       and other personnel in the aftermath of an emergency. Has your IHE considered strategies for</legend>
       
       
            <label for="rbtContinuityBussOperations">Continuity of business operations, such as ensuring that faculty and staff are paid, 
             and that your contractors and vendors are paid? 
            <asp:RequiredFieldValidator ID="reqrContinuityBussOperations" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtContinuityBussOperations"
                    ErrorMessage="a i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtContinuityBussOperations" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

         <label for="rbtContinuityTeaching">Continuity of teaching and learning, 
           such as distance learning or using another campus’s facilities?  

         <asp:RequiredFieldValidator ID="reqContinuityTeaching" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtContinuityTeaching"
                    ErrorMessage="a ii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
         </label>
        <asp:RadioButtonList ID="rbtContinuityTeaching" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

         <label for="rbtHelpingStudents">Helping students, faculty, and staff
            regain a sense of normalcy following a traumatic experience or event?   

         <asp:RequiredFieldValidator ID="reqHelpingStudents" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtHelpingStudents"
                    ErrorMessage="a iii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
        </label>
        <asp:RadioButtonList ID="rbtHelpingStudents" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

  </fieldset>

<fieldset class="assessForm">

            <legend for="rbtIHEconsiderStrategies">Have you or your IHE considered strategies to facilitate the 
                <a class="MouesoverPopup" onclick="showPos(event,'')">psychological recovery</a>
                 of the campus community in the aftermath of a traumatic event that impacts your facility? 
            <asp:RequiredFieldValidator ID="reqIHEconsiderStrategies" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtIHEconsiderStrategies"
                    ErrorMessage="b. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtIHEconsiderStrategies" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>    


 </fieldset>
<div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />
        
</div>
      <div style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">8/9</p></div>

     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

     <div id='PopUp' class="PopUp" ><span class="b-close"></span>
               
              <p><span>Psychological First Aid (PFA)</span> is an evidence-informed approach use by mental health and disaster response
                   workers to help individuals of all ages in the immediate aftermath of disaster 
                  and terrorism. PFA is designed for delivery in diverse settings, including general population 
                  shelters, mobile dining facilities, disaster assistance service centers, and acute care facilities.</p>
            </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

