﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="E_PlanningEduCommunity.aspx.cs" Inherits="E_PlanningEduCommunity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
     <script type="text/javascript">

         function showPos(event, text) {
             var el;
             el = document.getElementById('PopUp');
             // el.style.display = "block";
             $("#PopUp").bPopup();
         }

         function showPos2(event, text) {
             var el;
             el = document.getElementById('PopUp2');
             // el.style.display = "block";
             $("#PopUp2").bPopup();
         }
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
<h1>Planning for the Whole Higher Education Community </h1>

<fieldset class="assessForm">  
    <legend>Have you or your IHE considered how you’ll address the needs of those with 
        <a class="MouesoverPopup" onclick="showPos(event,'')">disabilities and other access and functional needs</a>
        during an emergency event?</legend>


            <label for="rbtAddressNeedsWithDisab">
            <asp:RequiredFieldValidator ID="reqAddressNeedsWithDisab" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAddressNeedsWithDisab"
                    ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>               
             <asp:RadioButtonList ID="rbtAddressNeedsWithDisab" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" >
             </asp:RadioButtonList>     


            
 </fieldset>

<fieldset class="assessForm">
    <legend>Emergency events may take place at different times and at locations other than your main campus
       buildings. Have you determined how you’ll respond to emergencies that take place </legend>
  
               <label for="rbtDiffLocation">At different locations besides your main campus buildings? 
                <asp:RequiredFieldValidator ID="reqDiffLocation" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtDiffLocation"
                    ErrorMessage="b i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                </label>
                <asp:RadioButtonList ID="rbtDiffLocation" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>

              <label for="rbtOffCampus">At off-campus locales?
              <asp:RequiredFieldValidator ID="reqOffCampus" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtOffCampus"
                    ErrorMessage="b ii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
              </label>
              <asp:RadioButtonList ID="rbtOffCampus" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
              </asp:RadioButtonList>

             <label for="rbtAlternateTimes">During alternate times, 
              such as when classes are not in session, or during sporting events?
             <asp:RequiredFieldValidator ID="reqAlternateTimes" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAlternateTimes"
                    ErrorMessage="b iii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
             <asp:RadioButtonList ID="rbtAlternateTimes" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

</fieldset>

<fieldset class="assessForm">

      <legend>Have you trained groups or individuals who are not part of your 
        regular faculty or staff, but are regular participants in the higher education setting, on responding
        to the different types of emergency events that may impact your institution and community? </legend>
    	
     <label for="rbtTrainedGroup">   
       <asp:RequiredFieldValidator ID="reqTrainedGroup" runat="server" 
         ValidationGroup="Save" ControlToValidate="rbtTrainedGroup"
         ErrorMessage="c. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>     
      </label>
        <asp:RadioButtonList ID="rbtTrainedGroup" runat="server" RepeatDirection="Vertical"  onclick="showPos2(event,'')"  RepeatLayout="Flow">
        </asp:RadioButtonList>     
    
  </fieldset>

 <div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" ValidationGroup="Save" Text="Next" OnClick="btnNext_Click" />
        
 </div>
      <div style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">5/9</p></div>
     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

          <div id='PopUp' class="PopUp">
               
               <p><span>A disability</span> is a physical or mental impairment that substantially limits one or more major life activities. 
                  Your plan must address the needs of students, faculty, staff, and visitors with disabilities
                   and other access and functional needs, including language, transportation, and medical needs.
                   Additional information on Americans with Disabilities Act (ADA) and Civil Rights Act compliance 
                  is available on page 23 of the Guide for Developing High-Quality Emergency Operations Plans for 
                  Institutions of Higher Education.</p>
             </div>
     <div id='PopUp2' class="PopUp" ><span class="b-close"></span>
                
              <span>Those groups or individuals who are not part of the regular staff may include substitute and part-time 
                  professors or adjunct instructors, bus drivers, merchants, food service personnel, facilities managers,
                   and seasonal program staff.</span>
             </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

