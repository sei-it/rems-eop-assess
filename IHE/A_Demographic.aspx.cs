﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class A_Demographic : System.Web.UI.Page
{
    int lngPkID;
    int intUserId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }

        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }

    protected void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.AssessmentQuestionID == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {


                objVal = oRes.DEMO_A_ORGANIZATION_FROM_FK;
                if (objVal != null)
                {
                    ddlOrganizationFrom.SelectedIndex = ddlOrganizationFrom.Items.IndexOf(ddlOrganizationFrom.Items.FindByValue(oRes.DEMO_A_ORGANIZATION_FROM_FK.ToString()));
                }
                objVal = oRes.DEMO_B_YOUR_ROLE_FK;
                if (objVal != null)
                {
                    ddlYourRole.SelectedIndex = ddlYourRole.Items.IndexOf(ddlYourRole.Items.FindByValue(oRes.DEMO_B_YOUR_ROLE_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.AssessmentQuestionID == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;

            }


            if (!(ddlOrganizationFrom.SelectedItem == null))
            {
                if (!(ddlOrganizationFrom.SelectedItem.Value.ToString() == "Select"))
                {
                    oRes.DEMO_A_ORGANIZATION_FROM_FK = Convert.ToInt32(ddlOrganizationFrom.SelectedItem.Value.ToString());
                }
               
            }
            if (!(ddlYourRole.SelectedItem == null))
            {
                if (!(ddlYourRole.SelectedItem.Value.ToString() == "Select"))
                {
                    oRes.DEMO_B_YOUR_ROLE_FK = Convert.ToInt32(ddlYourRole.SelectedValue.ToString());
                }
            }


            oRes.CreatedDate = DateTime.Now;


            if (blNew == true)
            {

                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);

            }

            db.SubmitChanges();
            lngPkID = oRes.AssessmentQuestionID;
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from e in db.A_DemoOrganizationFroms
                      select e;
            ddlOrganizationFrom.DataSource = qry;
            ddlOrganizationFrom.DataTextField = "aOrganizationNames";
            ddlOrganizationFrom.DataValueField = "aOrganizationFromId";
            ddlOrganizationFrom.DataBind();
            ddlOrganizationFrom.Items.Insert(0, "Select");


            var qry2 = from e1 in db.B_DemoYourRoles
                       select e1;

            ddlYourRole.DataSource = qry2;
            ddlYourRole.DataTextField = "bYourRoles";
            ddlYourRole.DataValueField = "bYourRoleId";
            ddlYourRole.DataBind();
            ddlYourRole.Items.Insert(0, "Select");



        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("B_Introduction.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {

        updateUser();
        displayRecords();
        Response.Redirect("Default.aspx?lngPkID=" + lngPkID);

    }
}