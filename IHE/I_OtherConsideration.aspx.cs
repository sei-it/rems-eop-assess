﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class I_OtherConsideration : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }



    protected void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.AssessmentQuestionID == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.OTHER_CONSIDERATIONS_A_APPRO_PERSONNEL_FK;
                if (objVal != null)
                {
                    rbtAppropriatePersonnel.SelectedIndex = rbtAppropriatePersonnel.Items.IndexOf(rbtAppropriatePersonnel.Items.FindByValue(oRes.RECOVERY_A1_CON_BUSINESS_OPERATIONS_FK.ToString()));
                }
                objVal = oRes.OTHER_CONSIDERATIONS_A_APPRO_PERSONNEL_FK;
                if (objVal != null)
                {
                    rbtAwareEmergencyResponse.SelectedIndex = rbtAwareEmergencyResponse.Items.IndexOf(rbtAwareEmergencyResponse.Items.FindByValue(oRes.OTHER_CONSIDERATIONS_B_AWARE_EMG_RESPONSE_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.AssessmentQuestionID == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;
            }

            if (!(rbtAppropriatePersonnel.SelectedItem == null))
            {
                if (!(rbtAppropriatePersonnel.SelectedItem.Value.ToString() == ""))
                {
                    oRes.OTHER_CONSIDERATIONS_A_APPRO_PERSONNEL_FK = Convert.ToInt32(rbtAppropriatePersonnel.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.OTHER_CONSIDERATIONS_A_APPRO_PERSONNEL_FK = null;
                }
            }
            if (!(rbtAwareEmergencyResponse.SelectedItem == null))
            {
                if (!(rbtAwareEmergencyResponse.SelectedItem.Value.ToString() == ""))
                {
                    oRes.OTHER_CONSIDERATIONS_B_AWARE_EMG_RESPONSE_FK = Convert.ToInt32(rbtAwareEmergencyResponse.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.OTHER_CONSIDERATIONS_B_AWARE_EMG_RESPONSE_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = oRes.AssessmentQuestionID;
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from e in db.YesNo_Nots
                      select e;

            rbtAppropriatePersonnel.DataSource = qry;
            rbtAppropriatePersonnel.DataTextField = "Yes_No_Not";
            rbtAppropriatePersonnel.DataValueField = "NotID";
            rbtAppropriatePersonnel.DataBind();

            var qry2 = from e in db.YesNos
                       select e;
            rbtAwareEmergencyResponse.DataSource = qry2;
            rbtAwareEmergencyResponse.DataTextField = "YES_NO";
            rbtAwareEmergencyResponse.DataValueField = "YESNOId";
            rbtAwareEmergencyResponse.DataBind();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("J_Conclusion.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("H_Recovery.aspx?lngPkID=" + lngPkID);
    }
}