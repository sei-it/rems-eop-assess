﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="C_ThreatHazards.aspx.cs" Inherits="C_ThreatHazards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
     <script type="text/javascript">

         function showPos3(event, text) {
             var el;
             el = document.getElementById('PopUp3');
             $("#PopUp3").bPopup();
             // el.style.display = "block";
         }
         function showPos4(event, text) {
             var el;
             el = document.getElementById('PopUp4');
             $("#PopUp4").bPopup();
             // el.style.display = "block";
         }

         function showPos5(event, text) {
             var el;
             el = document.getElementById('PopUp5');
             $("#PopUp5").bPopup();
             // el.style.display = "block";
         }

         function showPos6(event, text) {
             var el;
             el = document.getElementById('PopUp6');
             $("#PopUp6").bPopup();
             // el.style.display = "block";
         }

         function showPos7(event, text) {
             var el;
             el = document.getElementById('PopUp7');
             $("#PopUp7").bPopup();
             // el.style.display = "block";
         }

         function showPos8(event, text) {
             var el;
             el = document.getElementById('PopUp8');
             $("#PopUp8").bPopup();
             // el.style.display = "block";
         }

         function showPos9(event, text) {
             var el;
             el = document.getElementById('PopUp9');
             $("#PopUp9").bPopup();
             // el.style.display = "block";
         }

         function showPos10(event, text) {
             var el;
             el = document.getElementById('PopUp10');
             $("#PopUp10").bPopup();
             // el.style.display = "block";
         }

         function showPos11(event, text) {
             var el;
             el = document.getElementById('PopUp11');
             $("#PopUp11").bPopup();
             // el.style.display = "block";
         }

         function showPos12(event, text) {
             var el;
             el = document.getElementById('PopUp12');
             $("#PopUp12").bPopup();
             // el.style.display = "block";
         }

     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <h1>Threats and Hazards</h1>

<fieldset class="assessForm">  
  <legend>In addition to thinking about who should be on your planning team, let’s think about what types of emergency events you and your
         team will have to plan for. These are generally called
         <a class="MouesoverPopup" onclick="showPos3(event,'')"> "threats"</a>and
         <a class="MouesoverPopup" onclick="showPos4(event,'')"> "hazards."</a></legend>
           <label for="rbtListOfRiskThreats"> Have you or your IHE’s planning team thought about or made a list 
                     of the risks or threats and hazards most likely to impact your campus or community?
    
             <asp:RequiredFieldValidator ID="reqListOfRiskThreats" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtListOfRiskThreats"
                    ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
            <asp:RadioButtonList ID="rbtListOfRiskThreats" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList> 
     </fieldset>

<fieldset class="assessForm">   
    <legend>Whether or not your IHE has thought about or made a list of the risks or threats and hazards most likely 
        to impact your campus or surrounding community, have you considered strategies to:</legend>
    
                <label for="rbtPrevent"><a class="MouesoverPopup" onclick="showPos5(event,'')">Prevent</a> an emergency event from impacting your campus/IHE?
                  <asp:RequiredFieldValidator ID="reqPrevent" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtPrevent"
                    ErrorMessage="b i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                 </label>
                <asp:RadioButtonList ID="rbtPrevent" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
  

    
               <label for="rbtProtect"><a class="MouesoverPopup" onclick="showPos6(event,'')">Protect</a>  against the adverse effects of an emergency event? 
                <asp:RequiredFieldValidator ID="reqProtect" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtProtect"
                    ErrorMessage="b ii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                 </label>
                <asp:RadioButtonList ID="rbtProtect" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
    

  
             <label for="rbtMitigate"><a class="MouesoverPopup" onclick="showPos7(event,'')">Mitigate</a> the effects of an emergency event?
             <asp:RequiredFieldValidator ID="reqMitigate" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtMitigate"
                    ErrorMessage="b iii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
              </label>
             <asp:RadioButtonList ID="rbtMitigate" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
             </asp:RadioButtonList>
   
             <label for="rbtRespond"><a class="MouesoverPopup" onclick="showPos8(event,'')">Respond</a> to an emergency event? 
             <asp:RequiredFieldValidator ID="reqRespond" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtRespond"
                    ErrorMessage="b iv. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
             <asp:RadioButtonList ID="rbtRespond" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
             </asp:RadioButtonList>
    
               <label for="rbtRecover"><a class="MouesoverPopup" onclick="showPos9(event,'')">Recover</a> from an emergency event? 
               <asp:RequiredFieldValidator ID="reqRecover" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtRecover"
                    ErrorMessage="b v. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                </label>
                <asp:RadioButtonList ID="rbtRecover" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
     </fieldset>

<fieldset class="assessForm">  
  <legend>Your IHE may already employ strategies for responding to a variety of threats or hazards. For example, when you practice regular drills, such as fire drills, you are practicing 
      ways to respond to specific events. Does your IHE currently practice any of the following drills?</legend>
   
            <label for="rbtDenyEntry"><a class="MouesoverPopup" onclick="showPos10(event,'')">Deny Entry or Closing</a>: 
            <asp:RequiredFieldValidator ID="reqDenyEntry" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtDenyEntry"
                    ErrorMessage="c i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtDenyEntry" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" >
            </asp:RadioButtonList>
           
             <label for="rbtEvacuation"><a class="MouesoverPopup" onclick="showPos11(event,'')">Evacuation</a>:  
              <asp:RequiredFieldValidator ID="reqEvacuation" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtEvacuation"
                    ErrorMessage="c ii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
              </label>
              <asp:RadioButtonList ID="rbtEvacuation" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
              </asp:RadioButtonList>
       
              <label for="rbtShelterInPlace"><a class="MouesoverPopup" onclick="showPos12(event,'')">Shelter-in-place</a>:
               <asp:RequiredFieldValidator ID="reqShelterInPlace" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtShelterInPlace"
                    ErrorMessage="c iii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
               <asp:RadioButtonList ID="rbtShelterInPlace" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
               </asp:RadioButtonList>
      
     </fieldset>

    <div class="prevNext">
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" ValidationGroup="Save" Text="Next" OnClick="btnNext_Click" />
        
    </div>
      <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">3/9</p></div>
     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

     <div id='PopUp3' class="PopUp" ><span class="b-close"></span>
              <p><span>Threats</span> are human-caused emergencies such as fire, gang violence, cyber-attacks, and <em>active shooters</em>.</p>
              
    </div>
    <div id='PopUp4' class="PopUp" ><span class="b-close"></span>
              <p><span>Hazards</span> may be natural disasters, such as earthquakes, wildfires, and floods, technological, such as hazardous materials releases, and biological, such as infectious disease outbreaks.</p>
                
              
    </div>
     <div id='PopUp5' class="PopUp" ><span class="b-close"></span>
              <p><span>Prevention:</span> The action IHEs take to prevent a threatened 
                  or actual incident from occurring. For example, reviewing recent community and campus-specific crime data.</p>
              
    </div>
     <div id='PopUp6' class="PopUp" ><span class="b-close"></span>
              <p><span>Protection:</span> Protection focuses on ongoing actions that protect students, faculty, staff, visitors,
                   networks, and property from a threat or hazard. For example, conducting hurricane drills can reduce 
                  injury to students, faculty, and staff because they will know what to do to avoid harm. </p>
    </div>
     <div id='PopUp7' class="PopUp" ><span class="b-close"></span>
              <p><span>Mitigation:</span> The capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the impact of an 
                  event or emergency. For example, improving surveillance capabilities and access controls may mitigate some emergencies.</p>
              
    </div>
     <div id='PopUp8' class="PopUp" ><span class="b-close"></span>
              <p><span>Response:</span> The capabilities necessary to stabilize an emergency once it has already happened or is certain to happen 
                  in an unpreventable way. For example, evacuating or sheltering-in-place, as appropriate.</p>
    </div>
     <div id='PopUp9' class="PopUp" ><span class="b-close"></span>
              <p><span>Recovery:</span> The capabilities necessary to assist IHEs affected by an event or emergency in restoring the learning environment. For example, 
                  providing stress management and mental health resources to students, faculty, and staff following an emergency event.</p>
              
    </div>
     <div id='PopUp10' class="PopUp" ><span class="b-close"></span>
              <p><span>Deny Entry or Closing:</span> Secure IHE buildings, facilities, and
                   grounds during incidents that pose an immediate threat or violence in or around the IHE.</p>
              
    </div>
     <div id='PopUp11' class="PopUp" ><span class="b-close"></span>
              <p><span>Evacuation:</span> Evacuate IHE buildings and grounds.</p>
                
              
    </div>
     <div id='PopUp12' class="PopUp" ><span class="b-close"></span>
              <p><span>Shelter-in-place:</span> Students and staff remain indoors, perhaps for an extended period of time, because it is safer 
                  inside the building or a room than outside.</p>
                    
              
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

