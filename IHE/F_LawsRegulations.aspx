﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="F_LawsRegulations.aspx.cs" Inherits="F_LawsRegulations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    <script type="text/javascript">

        function showPos(event, text) {
            var el;
            el = document.getElementById('PopUp');
            // el.style.display = "block";
            $("#PopUp").bPopup();
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
<h1>Laws and Regulations</h1>

<fieldset class="assessForm">
    <legend>Are you aware that there are local, state, and Federal laws
       that relate to campus safety and that impact how you and your IHE should plan for emergencies?</legend>

     <asp:RequiredFieldValidator ID="reqLocalStateFederalLaw" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtLocalStateFederalLaw"
                    ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

       <label for="rbtLocalStateFederalLaw"></label>
        <asp:RadioButtonList ID="rbtLocalStateFederalLaw" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

</fieldset>
<fieldset class="assessForm">
  <legend>If you already have an emergency plan, 
      are you aware whether or not the plan already incorporates all applicable 
      <a class="MouesoverPopup" onclick="showPos(event,'')"><u>laws and regulations</u>? </a></legend>
       <asp:RequiredFieldValidator ID="reqAlreadyHaveEmgPlan" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAlreadyHaveEmgPlan"
                    ErrorMessage="b. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

       <label for="rbtAlreadyHaveEmgPlan"></label>
        <asp:RadioButtonList ID="rbtAlreadyHaveEmgPlan" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>      

          
 </fieldset>

            
   
 <div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />
        
 </div>
      <div style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">6/9</p></div>

     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>


     <div id='PopUp' class="PopUp" ><span class="b-close"></span>
              <span>Information on applicable laws and regulations may be located on local or 
                  state government webpages. Additional information on federal statutes that have 
                  implications for information sharing in the emergency planning process may be 
                  found in the IHE Resources section of the REMS TA Center website.</span>
            </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

