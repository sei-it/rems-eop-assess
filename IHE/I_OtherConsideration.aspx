﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="I_OtherConsideration.aspx.cs" Inherits="I_OtherConsideration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <h1>Other Considerations</h1>
    
<fieldset class="assessForm">
            <legend for="rbtAppropriatePersonnel">Do the appropriate personnel at your IHE have a clear understanding 
            of what student records may be shared with law enforcement, social workers, public health workers,
            etc. after an emergency, such as an outbreak of an infectious disease in the community?
     
             <asp:RequiredFieldValidator ID="reqAppropriatePersonnel" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAppropriatePersonnel"
                    ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtAppropriatePersonnel" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
 </fieldset>

<fieldset class="assessForm">
            <legend for="rbtAwareEmergencyResponse">Are you aware of other emergency response plans at your city or 
             county level that support and supplement your emergency preparedness efforts?
             <asp:RequiredFieldValidator ID="reqAwareEmergencyResponse" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAwareEmergencyResponse"
                    ErrorMessage="b. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtAwareEmergencyResponse" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

 </fieldset>
<div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next"  ValidationGroup="Save" OnClick="btnNext_Click" />
        
</div>
      <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">9/9</p></div>

      <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

