﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="G_TrainingDocumentation.aspx.cs" Inherits="G_TrainingDocumentation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    
<h1>Training and Documentation</h1>
  
<fieldset class="assessForm">
            <legend for="rbtAccessibleToolsHandy">Does your IHE have any accessible tools and documents handy that contain instructions
            for students, faculty, staff, and visitors for use during an emergency?

            <asp:RequiredFieldValidator ID="reqAccessibleToolsHandy" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAccessibleToolsHandy"
                    ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </legend>
             <asp:RadioButtonList ID="rbtAccessibleToolsHandy" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
      
   </fieldset>

<fieldset class="assessForm">
            <legend for="rbtIHEpostImp">Does your IHE post important information such as exit locations and 
             evacuation routes throughout the building and in plain view for use during an emergency event?
             <asp:RequiredFieldValidator ID="reqIHEpostImp" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtIHEpostImp"
                    ErrorMessage="b. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </legend>
            <asp:RadioButtonList ID="rbtIHEpostImp" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
    </fieldset>

<fieldset class="assessForm">
            <legend for="rbtAccessibleToolComply">Do the accessible tools, documents, and posted information mentioned above comply
            with the requirements put forth in the Americans with Disabilities Act (ADA) guidance?

            <asp:RequiredFieldValidator ID="reqAccessibleToolComply" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAccessibleToolComply"
                    ErrorMessage="c. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtAccessibleToolComply" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
       
  </fieldset>

<div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />
        
</div>
      <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">7/9</p></div>

     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

