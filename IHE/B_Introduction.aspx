﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="B_Introduction.aspx.cs" Inherits="B_Introduction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
     <script type="text/javascript">

         function showPosE(event, text) {
             var el;
             el = document.getElementById('PopUpE');
             // el.style.display = "block";
             $("#PopUpE").bPopup();
         }

         function showPos(event, text) {
             var el;
             el = document.getElementById('PopUp');
             // el.style.display = "block";
             $("#PopUp").bPopup();
         }

         function showPos2(event, text) {
             var el;
             el = document.getElementById('PopUp2');
             // el.style.display = "block";
             $("#PopUp2").bPopup();
         }


         function showPos3(event, text) {
             var el;
             el = document.getElementById('PopUp3');
             // el.style.display = "block";
             $("#PopUp3").bPopup();
         }
        
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
      <h1>Introduction</h1>

<fieldset class="assessForm">
               <legend for="rbtEmergencyPlan">Does your IHE currently have an <a class="MouesoverPopup" onclick="showPosE(event,'')"> emergency plan?</a> 
                 <asp:RequiredFieldValidator ID="reqYourRole" runat="server" 
                  ValidationGroup="Save" ControlToValidate="rbtEmergencyPlan"
                  ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator></legend>
              
               <asp:RadioButtonList ID="rbtEmergencyPlan" runat="server"  onclick="showPos(event,'')"  RepeatLayout="Flow">
               </asp:RadioButtonList>  
</fieldset>
<fieldset class="assessForm">
             <legend for="rbtIndividualTeamSafety">Does your IHE have an individual or team in charge of safety?
                    <asp:RequiredFieldValidator ID="reqIndividualTeamSafety" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtIndividualTeamSafety"
                    ErrorMessage="b. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator> </legend>        
                
            <asp:RadioButtonList ID="rbtIndividualTeamSafety" runat="server"  onclick="showPos2(event,'')"  RepeatLayout="Flow">
            </asp:RadioButtonList>                
</fieldset>
    
 <fieldset class="assessForm">

     <legend>Have you considered reaching out to the following community partners to 
                discuss collaborating on response strategies to an  <a class="MouesoverPopup" onclick="showPos3(event,'')"> emergency event?</a> </legend>      
           
            
            <label for="rbtFirstResponders">First responders?
                <asp:RequiredFieldValidator ID="reqFirstResponders" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtFirstResponders"
                    ErrorMessage="c i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>


            </label>
            <asp:RadioButtonList ID="rbtFirstResponders" runat="server"   RepeatLayout="Flow" >

            </asp:RadioButtonList>

             
          
          
            <label for="rbtLocalEmergency">Local emergency managers?
                <asp:RequiredFieldValidator ID="reqLocalEmergency" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtLocalEmergency"
                    ErrorMessage="c ii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

            </label>
            <asp:RadioButtonList ID="rbtLocalEmergency" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>


            <label for="rblLawEnforcement">Local law enforcement officers?
                 <asp:RequiredFieldValidator ID="reqLawEnforcement" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rblLawEnforcement"
                    ErrorMessage="c iii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rblLawEnforcement" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>


            <label for="rbtEMSpersonnel">Emergency medical services (EMS) personnel?

                 <asp:RequiredFieldValidator ID="reqEMSpersonnel" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtEMSpersonnel"
                    ErrorMessage="c iv. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtEMSpersonnel" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>


            <label for="rbtFireOfficials">Fire officials? 
                 <asp:RequiredFieldValidator ID="reqFireOfficials" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtFireOfficials"
                    ErrorMessage="c v. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

            </label>
            <asp:RadioButtonList ID="rbtFireOfficials" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>


            <label for="rbtHealthPractitioners">Public and mental health practitioners?

                  <asp:RequiredFieldValidator ID="reqHealthPractitioners" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtHealthPractitioners"
                    ErrorMessage="c vi. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

            </label>
            <asp:RadioButtonList ID="rbtHealthPractitioners" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>


                
            
         </fieldset>
    <div>
          <div class="prevNext">
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext"  ValidationGroup="Save" runat="server" Text="Next" OnClick="btnNext_Click" />
        
    	</div>
          <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">2/9</p></div>
          <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
         <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    </div>



            <div id='PopUpE' class="PopUp" ><span class="b-close"></span>
             <span> An emergency plan describes who will
              do what, as well as when, with what resources, and by what 
              authority--before, during, and immediately after an emergency.
             </span>        
            </div>
            <div id='PopUp' class="PopUp" ><span class="b-close"></span>
              <span>Whether or not you currently have a plan, you are in the right place! 
                  This assessment will help you recognize areas of higher education safety planning in which you
                   may already be engaged, and those in which you can expand or enhance, including recommendations 
                  for how to move forward on both.</span>
             </div>

            <div id='PopUp2' class="PopUp" ><span class="b-close"></span>
              <span>A high-quality emergency operations plan is created through a process that involves 
                  the contributions of many individuals and organizations who have a responsibility in 
                  keeping your campus and its community safe. Consider the following recommended roles 
                  for participation on your planning team.</span>
            </div>

            <div id='PopUp3' class="PopUp"><span class="b-close"></span>
              <span>Not only should these partners be a part of the planning process, but the final 
                  plan should be presented to the appropriate leadership for official approval, 
                  and shared with community partners who have a responsibility in carrying out the 
                  plan. Your IHE should also consider measures to protect the plan from those 
                  who are not authorized to have it.</span>
             </div>
    
             

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

