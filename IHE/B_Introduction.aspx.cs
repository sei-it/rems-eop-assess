﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class B_Introduction : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }


    protected void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.AssessmentQuestionID == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {



                objVal = oRes.INTRO_A_EMERGENCY_PLAN_FK;
                if (objVal != null)
                {
                    rbtEmergencyPlan.SelectedIndex = rbtEmergencyPlan.Items.IndexOf(rbtEmergencyPlan.Items.FindByValue(oRes.INTRO_A_EMERGENCY_PLAN_FK.ToString()));
                }
                objVal = oRes.INTRO_B_INDIVIDUAL_OR_TEAM_SAFETY_FK;
                if (objVal != null)
                {
                    rbtIndividualTeamSafety.SelectedIndex = rbtIndividualTeamSafety.Items.IndexOf(rbtIndividualTeamSafety.Items.FindByValue(oRes.INTRO_B_INDIVIDUAL_OR_TEAM_SAFETY_FK.ToString()));
                }
                objVal = oRes.INTRO_C1_FIRST_RESPONDERS_FK;
                if (objVal != null)
                {
                    rbtFirstResponders.SelectedIndex = rbtFirstResponders.Items.IndexOf(rbtFirstResponders.Items.FindByValue(oRes.INTRO_C1_FIRST_RESPONDERS_FK.ToString()));
                }
                objVal = oRes.INTRO_C2_LOCAL_EMERGENCY_MNG_FK;
                if (objVal != null)
                {
                    rbtLocalEmergency.SelectedIndex = rbtLocalEmergency.Items.IndexOf(rbtLocalEmergency.Items.FindByValue(oRes.INTRO_C2_LOCAL_EMERGENCY_MNG_FK.ToString()));
                }
                objVal = oRes.INTRO_C3_LOCAL_LAW_ENFORCEMENT_FK;
                if (objVal != null)
                {
                    rblLawEnforcement.SelectedIndex = rblLawEnforcement.Items.IndexOf(rblLawEnforcement.Items.FindByValue(oRes.INTRO_C3_LOCAL_LAW_ENFORCEMENT_FK.ToString()));

                }
                objVal = oRes.INTRO_C4_EMS_PERSONNEL_FK;
                if (objVal != null)
                {
                    rbtEMSpersonnel.SelectedIndex = rbtEMSpersonnel.Items.IndexOf(rbtEMSpersonnel.Items.FindByValue(oRes.INTRO_C4_EMS_PERSONNEL_FK.ToString()));
                }
                objVal = oRes.INTRO_C5_FIRE_OFFICALS_FK;
                if (objVal != null)
                {
                    rbtFireOfficials.SelectedIndex = rbtFireOfficials.Items.IndexOf(rbtFireOfficials.Items.FindByValue(oRes.INTRO_C5_FIRE_OFFICALS_FK.ToString()));
                }
                objVal = oRes.INTRO_C6_PUBLIC_MENTAL_PRACTITIONERS_FK;
                if (objVal != null)
                {
                    rbtHealthPractitioners.SelectedIndex = rbtHealthPractitioners.Items.IndexOf(rbtHealthPractitioners.Items.FindByValue(oRes.INTRO_C6_PUBLIC_MENTAL_PRACTITIONERS_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.AssessmentQuestionID == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;

            }

            if (!(rbtEmergencyPlan.SelectedItem == null))
            {
                if (!(rbtEmergencyPlan.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_A_EMERGENCY_PLAN_FK = Convert.ToInt32(rbtEmergencyPlan.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_A_EMERGENCY_PLAN_FK = null;
                }
            }
            if (!(rbtIndividualTeamSafety.SelectedItem == null))
            {
                if (!(rbtIndividualTeamSafety.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_B_INDIVIDUAL_OR_TEAM_SAFETY_FK = Convert.ToInt32(rbtIndividualTeamSafety.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_B_INDIVIDUAL_OR_TEAM_SAFETY_FK = null;
                }
            }
            if (!(rbtFirstResponders.SelectedItem == null))
            {
                if (!(rbtFirstResponders.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C1_FIRST_RESPONDERS_FK = Convert.ToInt32(rbtFirstResponders.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C1_FIRST_RESPONDERS_FK = null;
                }
            }
            if (!(rbtLocalEmergency.SelectedItem == null))
            {
                if (!(rbtLocalEmergency.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C2_LOCAL_EMERGENCY_MNG_FK = Convert.ToInt32(rbtLocalEmergency.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C2_LOCAL_EMERGENCY_MNG_FK = null;
                }
            }
            if (!(rblLawEnforcement.SelectedItem == null))
            {
                if (!(rblLawEnforcement.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C3_LOCAL_LAW_ENFORCEMENT_FK = Convert.ToInt32(rblLawEnforcement.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C3_LOCAL_LAW_ENFORCEMENT_FK = null;
                }
            }
            if (!(rbtEMSpersonnel.SelectedItem == null))
            {
                if (!(rbtEMSpersonnel.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C4_EMS_PERSONNEL_FK = Convert.ToInt32(rbtEMSpersonnel.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C4_EMS_PERSONNEL_FK = null;
                }
            }
            if (!(rbtFireOfficials.SelectedItem == null))
            {
                if (!(rbtFireOfficials.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C5_FIRE_OFFICALS_FK = Convert.ToInt32(rbtFireOfficials.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C5_FIRE_OFFICALS_FK = null;
                }
            }
            if (!(rbtHealthPractitioners.SelectedItem == null))
            {
                if (!(rbtHealthPractitioners.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C6_PUBLIC_MENTAL_PRACTITIONERS_FK = Convert.ToInt32(rbtHealthPractitioners.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C6_PUBLIC_MENTAL_PRACTITIONERS_FK = null;
                }
            }


            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = oRes.AssessmentQuestionID;
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from e in db.YesNo_Nots
                      select e;


            rbtFirstResponders.DataSource = qry;
            rbtFirstResponders.DataTextField = "Yes_No_Not";
            rbtFirstResponders.DataValueField = "NotID";
            rbtFirstResponders.DataBind();

            rbtEmergencyPlan.DataSource = qry;
            rbtEmergencyPlan.DataTextField = "Yes_No_Not";
            rbtEmergencyPlan.DataValueField = "NotID";
            rbtEmergencyPlan.DataBind();

            rbtIndividualTeamSafety.DataSource = qry;
            rbtIndividualTeamSafety.DataTextField = "Yes_No_Not";
            rbtIndividualTeamSafety.DataValueField = "NotID";
            rbtIndividualTeamSafety.DataBind();

            rbtFirstResponders.DataSource = qry;
            rbtIndividualTeamSafety.DataTextField = "Yes_No_Not";
            rbtIndividualTeamSafety.DataValueField = "NotID";
            rbtIndividualTeamSafety.DataBind();

            rbtLocalEmergency.DataSource = qry;
            rbtLocalEmergency.DataTextField = "Yes_No_Not";
            rbtLocalEmergency.DataValueField = "NotID";
            rbtLocalEmergency.DataBind();

            rblLawEnforcement.DataSource = qry;
            rblLawEnforcement.DataTextField = "Yes_No_Not";
            rblLawEnforcement.DataValueField = "NotID";
            rblLawEnforcement.DataBind();

            rbtEMSpersonnel.DataSource = qry;
            rbtEMSpersonnel.DataTextField = "Yes_No_Not";
            rbtEMSpersonnel.DataValueField = "NotID";
            rbtEMSpersonnel.DataBind();

            rbtFireOfficials.DataSource = qry;
            rbtFireOfficials.DataTextField = "Yes_No_Not";
            rbtFireOfficials.DataValueField = "NotID";
            rbtFireOfficials.DataBind();

            rbtHealthPractitioners.DataSource = qry;
            rbtHealthPractitioners.DataTextField = "Yes_No_Not";
            rbtHealthPractitioners.DataValueField = "NotID";
            rbtHealthPractitioners.DataBind();


        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("C_ThreatHazards.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("A_Demographic.aspx?lngPkID=" + lngPkID);
    }
}