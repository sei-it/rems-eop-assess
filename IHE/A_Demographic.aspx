﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="A_Demographic.aspx.cs" Inherits="A_Demographic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
   <h1>Demographic Information</h1>

 <fieldset class="assessForm">   
           <legend for="ddlOrganizationFrom">What type of organization are you from?
           </legend>
           <asp:DropDownList ID="ddlOrganizationFrom" runat="server" RepeatDirection="Horizontal"  >          
           </asp:DropDownList> 

           <asp:RequiredFieldValidator ID="reqOrganizationFrom" runat="server" InitialValue="Select"
                ValidationGroup="Save" ControlToValidate="ddlOrganizationFrom"
                 ErrorMessage="a. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
 </fieldset>
<fieldset class="assessForm">       
             <legend for="ddlYourRole">What is your role, office affiliation, 
               or function within your IHE?  </legend>
              <asp:DropDownList ID="ddlYourRole" runat="server" RepeatDirection="Horizontal"  AutoPostBack="true">               
              </asp:DropDownList> 

             <asp:RequiredFieldValidator ID="reqYourRole" runat="server" InitialValue="Select"
                ValidationGroup="Save" ControlToValidate="ddlYourRole"
                 ErrorMessage="b. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
 </fieldset>
 <div class="prevNext">
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server"  ValidationGroup="Save" Text="Next" OnClick="btnNext_Click" />
        
 </div>
      <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">1/9</p></div>

     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />   
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

