﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="D_AccountingCommunication.aspx.cs" Inherits="D_AccountingCommunication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
<h1>Accounting and Communication</h1>

<fieldset class="assessForm">
        <legend>Do you have a designated person(s) for ensuring  that all students, staff, 
      faculty, and visitors are accounted for </legend>
    

            <label for="rbtBeforeAnEmergency">Before an emergency event?
            <asp:RequiredFieldValidator ID="reqBeforeEmergency" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtBeforeEmergency"
                    ErrorMessage="a i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtBeforeEmergency" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

            <label for="rbtDuringEmergency">During an emergency event?
            <asp:RequiredFieldValidator ID="reqDuringEmergency" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtDuringEmergency"
                    ErrorMessage="a ii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtDuringEmergency" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

            <label for="rbtAfterEmergency">After an emergency event? 
            <asp:RequiredFieldValidator ID="reqAfterEmergency" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAfterEmergency"
                    ErrorMessage="a iii. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
             <asp:RadioButtonList ID="rbtAfterEmergency" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
             </asp:RadioButtonList>

   </fieldset>

<fieldset class="assessForm">
     <legend>Before, during, and after an emergency, it is crucial to 
        be able to communicate with others. Do you have a designated person(s) for </legend> 
     
            <label for="rbtAnnouncing">Announcing what procedures to follow during an emergency event?
             <asp:RequiredFieldValidator ID="reqAnnouncing" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAnnouncing"
                    ErrorMessage="b i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtAnnouncing" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

            <label for="rbtCommunicating">Communicating with the press, media, and families following an emergency event?
            <asp:RequiredFieldValidator ID="reqCommunicating" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtCommunicating"
                    ErrorMessage="b i. is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtCommunicating" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

  </fieldset>

    <div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server"  ValidationGroup="Save" Text="Next" OnClick="btnNext_Click" />
        
    </div>
      <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">4/9</p></div>

     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

