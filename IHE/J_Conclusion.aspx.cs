﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class J_Conclusion : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
        }

        if (ViewState["IsLoaded1"] == null)
        {
            //displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("I_OtherConsideration.aspx?lngPkID=" + lngPkID);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        string strPath = Server.MapPath("~/doc/IHE Assessment Tool Output Report Formatted.docx");
        System.IO.FileInfo file = new System.IO.FileInfo(strPath);
        if (file.Exists == true)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "inline; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/doc";
            Response.WriteFile(file.FullName);
            Response.End();
            Response.Flush();
        }        
    }
}