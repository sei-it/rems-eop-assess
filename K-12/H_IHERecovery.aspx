﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="H_IHERecovery.aspx.cs" Inherits="H_IHERecovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" runat="Server">
    <h1>Continuity of Operations and Recovery</h1>

    <fieldset class="assessForm multiPart">
        <legend>17. It is important that your IHE is able to continue essential services during an emergency and its immediate 
            aftermath in order to normalize operations as quickly as possible. This will enable your IHE to maintain the 
            safety and emotional well-being of students, faculty, staff, and the learning environment in the aftermath 
            of an emergency. Has your IHE considered strategies for:</legend>
        <div class="questionGroup">

            <label for="rbtContinuityBussOperations">
                <a class="MouesoverPopup">Continuity of business operations</a>, such as ensuring that faculty and staff are paid, 
             and that your contractors and vendors are paid? 
            <asp:RequiredFieldValidator ID="reqrContinuityBussOperations" runat="server"
                ValidationGroup="Save" ControlToValidate="rbtContinuityBussOperations"
                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtContinuityBussOperations" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
        </div>
        <!-- /questionGroup -->

        <div class="questionGroup">
            <label for="rbtContinuityTeaching">
                <a class="MouesoverPopup">Continuity of teaching and learning</a>, 
           such as distance learning or using another campus’s facilities?  

         <asp:RequiredFieldValidator ID="reqContinuityTeaching" runat="server"
             ValidationGroup="Save" ControlToValidate="rbtContinuityTeaching"
             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtContinuityTeaching" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
        </div>
        <!-- /questionGroup -->

        <div class="questionGroup">
            <label for="rbtHelpingStudents">
                <a class="MouesoverPopup">Helping students, faculty, and staff
            regain a sense of normalcy</a> following a traumatic experience or event?   

         <asp:RequiredFieldValidator ID="reqHelpingStudents" runat="server"
             ValidationGroup="Save" ControlToValidate="rbtHelpingStudents"
             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtHelpingStudents" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
        </div>
        <!-- /questionGroup -->


    </fieldset>

    <fieldset class="assessForm">

        <legend for="rbtIHEconsiderStrategies">18. Have you or your IHE considered strategies to facilitate the 
                <a class="MouesoverPopup">psychological recovery</a>
            of the campus community in the aftermath of a traumatic event that impacts your facility? 
            <asp:RequiredFieldValidator ID="reqIHEconsiderStrategies" runat="server"
                ValidationGroup="Save" ControlToValidate="rbtIHEconsiderStrategies"
                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
        </legend>
        <asp:RadioButtonList ID="rbtIHEconsiderStrategies" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

        <label class="noBorder1" for="rbtIHEconsiderStrategies"><i>Psychological First Aid (PFA) is an evidence-informed approach use by mental health and disaster response workers to help individuals of all ages in the immediate aftermath of disaster and terrorism. PFA is designed for delivery in diverse settings, including general population shelters, mobile dining facilities, disaster assistance service centers, and acute care facilities.</i></label>
    </fieldset>
    <div class="prevNext">
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNextIHE_Click" />

    </div>

    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" CssClass="ValidationSummary"
        HeaderText="All required information was not provided. Please answer the marked question(s)." />
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    <div id='PopUp' class="PopUp">
        <span class="b-close"></span>

        <p>
            <span>Psychological First Aid (PFA)</span> is an evidence-informed approach use by mental health and disaster response
                   workers to help individuals of all ages in the immediate aftermath of disaster 
                  and terrorism. PFA is designed for delivery in diverse settings, including general population 
                  shelters, mobile dining facilities, disaster assistance service centers, and acute care facilities.
        </p>
    </div>
</asp:Content>

