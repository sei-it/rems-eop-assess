﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="I_IHEOtherConsideration.aspx.cs" Inherits="I_IHEOtherConsideration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
     <h1>Other Considerations</h1>
    
<fieldset class="assessForm">
            <legend for="rbtAppropriatePersonnel">19. Do the appropriate personnel at your IHE have a clear understanding 
            of what student records may be shared with law enforcement, social workers, public health workers,
            etc. after an emergency, such as an outbreak of an infectious disease in the community?
     
             <asp:RequiredFieldValidator ID="reqAppropriatePersonnel" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAppropriatePersonnel"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtAppropriatePersonnel" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
 </fieldset>

<fieldset class="assessForm">
            <legend for="rbtAwareEmergencyResponse">20. Are you aware of other emergency response plans at your city or 
             county level that support and supplement your emergency preparedness efforts?
             <asp:RequiredFieldValidator ID="reqAwareEmergencyResponse" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAwareEmergencyResponse"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtAwareEmergencyResponse" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

 </fieldset>
<div class="prevNext">       
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE" runat="server" Text="Next"  ValidationGroup="Save" OnClick="btnNextIHE_Click" />
        
</div>
    

      <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" CssClass="ValidationSummary"
         HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>
</asp:Content>


