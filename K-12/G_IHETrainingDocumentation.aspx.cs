﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class G_IHETrainingDocumentation : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "7/10";
    }


    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.TRAINING_DOC_A_SCHOOL_ACCESSIBLE_TOOL_HANDY_FK;
                if (objVal != null)
                {
                    rbtAccessibleToolsHandy.SelectedIndex = rbtAccessibleToolsHandy.Items.IndexOf(rbtAccessibleToolsHandy.Items.FindByValue(oRes.TRAINING_DOC_A_SCHOOL_ACCESSIBLE_TOOL_HANDY_FK.ToString()));
                }
                objVal = oRes.TRAINING_DOC_B_SCHOOL_POST_IMP_INFO_FK;
                if (objVal != null)
                {
                    rbtIHEpostImp.SelectedIndex = rbtIHEpostImp.Items.IndexOf(rbtIHEpostImp.Items.FindByValue(oRes.TRAINING_DOC_B_SCHOOL_POST_IMP_INFO_FK.ToString()));
                }
                objVal = oRes.TRAINING_DOC_C_ADA_GUIDANCE_FK;
                if (objVal != null)
                {
                    rbtAccessibleToolComply.SelectedIndex = rbtAccessibleToolComply.Items.IndexOf(rbtAccessibleToolComply.Items.FindByValue(oRes.TRAINING_DOC_C_ADA_GUIDANCE_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.IHE_K12_FK == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;
            }

            if (!(rbtAccessibleToolsHandy.SelectedItem == null))
            {
                if (!(rbtAccessibleToolsHandy.SelectedItem.Value.ToString() == ""))
                {
                    oRes.TRAINING_DOC_A_SCHOOL_ACCESSIBLE_TOOL_HANDY_FK = Convert.ToInt32(rbtAccessibleToolsHandy.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.TRAINING_DOC_A_SCHOOL_ACCESSIBLE_TOOL_HANDY_FK = null;
                }
            }
            if (!(rbtIHEpostImp.SelectedItem == null))
            {
                if (!(rbtIHEpostImp.SelectedItem.Value.ToString() == ""))
                {
                    oRes.TRAINING_DOC_B_SCHOOL_POST_IMP_INFO_FK = Convert.ToInt32(rbtIHEpostImp.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.TRAINING_DOC_B_SCHOOL_POST_IMP_INFO_FK = null;
                }
            }
            if (!(rbtAccessibleToolComply.SelectedItem == null))
            {
                if (!(rbtAccessibleToolComply.SelectedItem.Value.ToString() == ""))
                {
                    oRes.TRAINING_DOC_C_ADA_GUIDANCE_FK = Convert.ToInt32(rbtAccessibleToolComply.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.TRAINING_DOC_C_ADA_GUIDANCE_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.YesNo_Nots
                      select e;

            rbtAccessibleToolsHandy.DataSource = qry;
            rbtAccessibleToolsHandy.DataTextField = "Yes_No_Not";
            rbtAccessibleToolsHandy.DataValueField = "NotID";
            rbtAccessibleToolsHandy.DataBind();

            rbtIHEpostImp.DataSource = qry;
            rbtIHEpostImp.DataTextField = "Yes_No_Not";
            rbtIHEpostImp.DataValueField = "NotID";
            rbtIHEpostImp.DataBind();

            rbtAccessibleToolComply.DataSource = qry;
            rbtAccessibleToolComply.DataTextField = "Yes_No_Not";
            rbtAccessibleToolComply.DataValueField = "NotID";
            rbtAccessibleToolComply.DataBind();


        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNextIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("H_IHERecovery.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBackIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("F_IHELawsRegulations.aspx?lngPkID=" + lngPkID);
    }
}