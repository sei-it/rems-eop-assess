﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="F_LawsRegulations.aspx.cs" Inherits="F_LawsRegulations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div id="active6"></div>
     <h1>Laws and Regulations</h1>

<fieldset class="assessForm"> 
  <legend for="rbtLocalStateFederalLaw" >12. Are you aware that there are local, state, and Federal laws
       that relate to school safety and that impact how you and your school should plan for emergencies?

      <asp:RequiredFieldValidator ID="reqLocalStateFederalLaw" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtLocalStateFederalLaw"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
  </legend>
 

    
        <asp:RadioButtonList ID="rbtLocalStateFederalLaw" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">

        </asp:RadioButtonList>

  </fieldset>

<fieldset class="assessForm">  
  <legend>13. If you already have an emergency plan, 
      are you aware of whether or not the plan incorporates all applicable
      <a class="MouesoverPopup" >laws and regulations? </a> <label>  </label><asp:RequiredFieldValidator ID="reqAlreadyHaveEmgPlan" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAlreadyHaveEmgPlan"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

  </legend>
 

       <label for="rbtAlreadyHaveEmgPlan"></label>
        <asp:RadioButtonList ID="rbtAlreadyHaveEmgPlan" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>      

        <label class="noBorder1" for="rbtAddressNeedsWithDisab"><i>Information on applicable laws and regulations may be located on local or state government webpages. Additional information on federal statutes that have implications for information sharing in the emergency planning process may be found in the K-12 Key Topics section of the REMS TA Center website.</i>
    
      </label>
    </fieldset>
    <div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />
       
        
   <%-- </div>
     <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">7/11</p></div>--%>
        <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" ValidationGroup="Save" runat="server" ForeColor="Red" 
              HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    <div id='PopUp' class="PopUp" ><span class="b-close"></span>
              <span>Information on applicable laws and regulations may be located on local or state government 
                  webpages. Additional information on federal statutes that have implications for information 
                  sharing in the emergency planning process may be found in the K-12 Key Topics section of the 
                  REMS TA Center website</span>
            </div>
</asp:Content>

