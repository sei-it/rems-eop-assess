﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class I_OtherConsideration : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "9/10";
    }



    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.OTHER_CONSIDERATIONS_AK12_APPRO_PERSONNEL_FK;
                if (objVal != null)
                {
                    rbtAppropriatePersonnel.SelectedIndex = rbtAppropriatePersonnel.Items.IndexOf(rbtAppropriatePersonnel.Items.FindByValue(oRes.OTHER_CONSIDERATIONS_AK12_APPRO_PERSONNEL_FK.ToString()));
                }
                objVal = oRes.OTHER_CONSIDERATIONS_BK12_AWARE_EMG_RESPONSE_FK;
                if (objVal != null)
                {
                    rbtAwareEmergencyResponse.SelectedIndex = rbtAwareEmergencyResponse.Items.IndexOf(rbtAwareEmergencyResponse.Items.FindByValue(oRes.OTHER_CONSIDERATIONS_BK12_AWARE_EMG_RESPONSE_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            K12_ASSESSMENT_QUESTION oRes = (from c in db.K12_ASSESSMENT_QUESTIONs
                                            where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new K12_ASSESSMENT_QUESTION();
                blNew = true;

            }

            if (!(rbtAppropriatePersonnel.SelectedItem == null))
            {
                if (!(rbtAppropriatePersonnel.SelectedItem.Value.ToString() == ""))
                {
                    oRes.OTHER_CONSIDERATIONS_AK12_APPRO_PERSONNEL_FK = Convert.ToInt32(rbtAppropriatePersonnel.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.OTHER_CONSIDERATIONS_AK12_APPRO_PERSONNEL_FK = null;
                }
            }
            if (!(rbtAwareEmergencyResponse.SelectedItem == null))
            {
                if (!(rbtAwareEmergencyResponse.SelectedItem.Value.ToString() == ""))
                {
                    oRes.OTHER_CONSIDERATIONS_BK12_AWARE_EMG_RESPONSE_FK = Convert.ToInt32(rbtAwareEmergencyResponse.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.OTHER_CONSIDERATIONS_BK12_AWARE_EMG_RESPONSE_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;
            oRes.Survey_Completed = 1;

            if (blNew == true)
            {
                db.K12_ASSESSMENT_QUESTIONs.InsertOnSubmit(oRes);
            }
            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.K12_YESNO_NOTs
                      select e;

            rbtAppropriatePersonnel.DataSource = qry;
            rbtAppropriatePersonnel.DataTextField = "Yes_No_Not";
            rbtAppropriatePersonnel.DataValueField = "NotID";
            rbtAppropriatePersonnel.DataBind();

            var qry2 = from e in db.k12_YESNOs
                       select e;
            rbtAwareEmergencyResponse.DataSource = qry2;
            rbtAwareEmergencyResponse.DataTextField = "YES_NO";
            rbtAwareEmergencyResponse.DataValueField = "YESNOId";
            rbtAwareEmergencyResponse.DataBind();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("J_Conclusion.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("H_Recovery.aspx?lngPkID=" + lngPkID);
    }
}