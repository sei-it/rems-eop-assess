﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="E_IHEPlanningEduCommunity.aspx.cs" Inherits="E_IHEPlanningEduCommunity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <h1>Planning for the Whole Higher Education Community </h1>

<fieldset class="assessForm">  
    <legend for="rbtAddressNeedsWithDisab">9. Have you or your IHE considered how you’ll address the needs of those with 
        <a class="MouesoverPopup" >disabilities and others with access and functional needs</a>
        during an emergency event?

            <asp:RequiredFieldValidator ID="reqAddressNeedsWithDisab" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAddressNeedsWithDisab"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
           </legend>             
             <asp:RadioButtonList ID="rbtAddressNeedsWithDisab" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" >
             </asp:RadioButtonList>     

    <label class="noBorder1" for="rbtAddressNeedsWithDisab"><i>A disability is a physical or mental impairment that substantially limits one or more major life activities. Your plan must address the needs of students, faculty, staff, and visitors with disabilities and other access and functional needs, including language, transportation, and medical needs. Additional information on Americans with Disabilities Act (ADA) and Civil Rights Act compliance is available on page 23 of the Guide for Developing High-Quality Emergency Operations Plans for Institutions of Higher Education.</i></label>
            
 </fieldset>

<fieldset class="assessForm multiPart">
    <legend>10. Emergency events may take place at <a class="MouesoverPopup" >different times</a> and at <a class="MouesoverPopup" >locations other than your main campus
       buildings</a>. Have you determined how you’ll respond to emergencies that take place:</legend>
  <div class="questionGroup">
               <label for="rbtDiffLocation">At different locations besides your main campus buildings? 
                <asp:RequiredFieldValidator ID="reqDiffLocation" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtDiffLocation"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                </label>
                <asp:RadioButtonList ID="rbtDiffLocation" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
    </div><!-- /questionGroup -->

			<div class="questionGroup">
              <label for="rbtOffCampus">At off-campus sites or satellite locations?
              <asp:RequiredFieldValidator ID="reqOffCampus" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtOffCampus"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
              </label>
              <asp:RadioButtonList ID="rbtOffCampus" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
              </asp:RadioButtonList>
</div><!-- /questionGroup -->

			<div class="questionGroup">
             <label for="rbtAlternateTimes">During alternate times, 
              such as when classes are not in session, or during sporting events?
             <asp:RequiredFieldValidator ID="reqAlternateTimes" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAlternateTimes"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
             <asp:RadioButtonList ID="rbtAlternateTimes" runat="server" CssClass="leftAlign"  RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
</div><!-- /questionGroup -->
</fieldset>

<fieldset class="assessForm">

      <legend for="rbtTrainedGroup">11. Have you trained groups or individuals who are not part of your 
        regular faculty or staff, but are regular participants in the higher education setting, on responding
        to the different types of emergency events that may impact your institution and community? 
    	
    
       <asp:RequiredFieldValidator ID="reqTrainedGroup" runat="server" 
         ValidationGroup="Save" ControlToValidate="rbtTrainedGroup"
         ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>     
     </legend>
        <asp:RadioButtonList ID="rbtTrainedGroup" runat="server" CssClass="leftAlign" RepeatDirection="Vertical"  RepeatLayout="Flow">
        </asp:RadioButtonList>     
    
      <label class="noBorder1" for="rbtAddressNeedsWithDisab"><i>Those groups or individuals who are not part of the regular staff may include substitute and part-time professors or adjunct instructors, bus drivers, merchants, food service personnel, facilities managers, and seasonal program staff.</i></label>
   
  </fieldset>

 <div class="prevNext">       
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE" runat="server" ValidationGroup="Save" Text="Next" OnClick="btnNextIHE_Click" />
        
 </div>
     
     <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" ValidationGroup="Save" runat="server" ForeColor="Red" 
       HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

          <div id='PopUp' class="PopUp"><span class="b-close"></span>
               
               <p><span>A disability</span> is a physical or mental impairment that substantially limits one or more major life activities. 
                  Your plan must address the needs of students, faculty, staff, and visitors with disabilities
                   and other access and functional needs, including language, transportation, and medical needs.
                   Additional information on Americans with Disabilities Act (ADA) and Civil Rights Act compliance 
                  is available on page 23 of the Guide for Developing High-Quality Emergency Operations Plans for 
                  Institutions of Higher Education.</p>
             </div>
     <div id='PopUp2' class="PopUp" ><span class="b-close"></span>
                
              <span>Those groups or individuals who are not part of the regular staff may include substitute and part-time 
                  professors or adjunct instructors, bus drivers, merchants, food service personnel, facilities managers,
                   and seasonal program staff.</span>
             </div>

</asp:Content>


