﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
     <%--<h1>INTRODUCTORY TEXT </h1>--%>
 
 <fieldset class="assessFormIntro">
 	<div id="active1"></div><!-- /active1 -->
    <h1>Welcome to EOP ASSESS!</h1>
     <p> This tool is designed to help you assess your capacity to create and 
         maintain a high-quality school or institution of higher education emergency 
         operations plan that considers a range of threats and hazards. The tool 
         contains questions that will assess your current understanding of your 
         organization’s plan, and help identify areas for learning opportunities, 
         while also taking you through a planning process recommended by the U.S. 
         Departments of Education, Homeland Security, Justice, and Health and Human 
         Services. Upon completing the self-assessment, your responses will result 
         in a customized downloadable report containing a variety of additional 
         resources and information relevant to your needs. This tool should take 
         approximately 12-15 minutes to complete. Please note that no information 
         is recorded during this process; your responses are used only to generate 
         your customized report, and no data remains in our system upon completion 
         and download of the report. </p>
    <p>For assistance using this tool, please contact the Readiness and Emergency Management for Schools (REMS) 
    Technical Assistance (TA) Center Help Desk by email <a href="mailto:info@remstacenter.org"> (info@remstacenter.org)</a>
     or by telephone (855-781-7367 [REMS]), toll free and between the hours of 9:00 a.m. and 5:00 p.m., Eastern Standard Time.</p>
<p>Let’s get started! Please note that all fields are required. Click NEXT to continue.</p>

    
   </fieldset>
    <div class="prevNext">
        <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" />
    </div>
     <div style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">1/11</p></div>



  
</asp:Content>

