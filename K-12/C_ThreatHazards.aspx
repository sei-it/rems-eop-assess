﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="C_ThreatHazards.aspx.cs" Inherits="C_ThreatHazards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>

    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div id="active3"></div>
     <h1>Threats and Hazards</h1>

<p> In addition to thinking about who should be on your planning team, let’s think about what types of emergency events you and your team will have to plan for. These are generally called "threats" and "hazards."</p>

<fieldset class="assessForm multiPart">

       <label for="rbtListOfRiskThreats">
            <legend>4. Have you or your school's planning team thought about or made a list of the threats and hazards most likely to impact your school or surrounding community? </legend>
           
            <asp:RequiredFieldValidator ID="reqListOfRiskThreats" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtListOfRiskThreats"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

       </label>
        <asp:RadioButtonList ID="rbtListOfRiskThreats" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow" >
            </asp:RadioButtonList> 
         
      <label  class="noBorder1" for="rbtEmergencyPlan" >Remember, threats are human-caused emergencies such as fire, gang violence, cyber-attacks, and active shooters, while hazards may be natural disasters, such as earthquakes, wildfires, and floods, technological, such as hazardous materials releases, and biological, such as infectious disease outbreaks.
    </label>


</fieldset>
    
<fieldset class="assessForm multiPart"> 
  <legend>5. Whether or not your school has thought about or made a list of the threats and hazards most likely 
      to impact your school or surrounding community, has your school considered strategies to address any of the following? </legend>
      
			<div class="questionGroup">
                   <label for="rbtPrevent"><a class="MouesoverPopup" >Prevent</a> an emergency event from impacting your school?
            
                    <asp:RequiredFieldValidator ID="reqPrevent" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtPrevent"
                                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            
                   </label>
                    <asp:RadioButtonList ID="rbtPrevent" runat="server" CssClass="leftAlign" RepeatDirection="Vertical"  RepeatLayout="Flow">         
                    </asp:RadioButtonList>
                    <label class="defined" for="rbtPrevent">Prevention is the action schools take to prevent a threatened or actual incident from occurring. For example, establishing access control procedures and providing IDs for students and staff might prevent a dangerous intruder from coming onto school grounds.</label>
			</div><!-- /questionGroup -->

			<div class="questionGroup">
                   <label for="rbtProtect"><a class="MouesoverPopup" >Protect</a> against the adverse effects of an emergency event? 
            
                    <asp:RequiredFieldValidator ID="reqProtect" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtProtect"
                                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            
                   </label>
                    <asp:RadioButtonList ID="rbtProtect" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
                    </asp:RadioButtonList>
               <label class="defined" for="rbtProtect"> Protection focuses on ongoing actions that protect students, teachers, staff, visitors, networks, and property from a threat or hazard. For example, conducting hurricane drills can reduce injury to students and staff because they will know what to do to avoid harm.</label >
			</div><!-- /questionGroup -->

			<div class="questionGroup">
                   <label for="rbtMitigate"><a class="MouesoverPopup" >Mitigate</a> the effects of an emergency event?
            
                       <asp:RequiredFieldValidator ID="reqMitigate" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtMitigate"
                                 ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                   </label>
                    <asp:RadioButtonList ID="rbtMitigate" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
                    </asp:RadioButtonList>
        
                    <label class="defined" for="rbtMitigate">Mitigation is the capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the impact of an event or emergency. For example, you may mitigate the impact of a possible earthquake by securing book-cases.</label>
			</div><!-- /questionGroup -->

                        <div class="questionGroup">
                   <label for="rbtRespond"><a class="MouesoverPopup" >Respond</a> to an emergency event? 
                        <asp:RequiredFieldValidator ID="reqRespond" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtRespond"
                                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                   </label>
                    <asp:RadioButtonList ID="rbtRespond" runat="server" CssClass="leftAlign"  RepeatDirection="Vertical" RepeatLayout="Flow">
            
                    </asp:RadioButtonList>
                <label class="defined" for="rbtRespond">Response is the capabilities necessary to stabilize an emergency once it has already happened or is certain to happen in an unpreventable way. For example, evacuating or locking down the school, as appropriate.
                </label>
			</div><!-- /questionGroup -->

			<div class="questionGroup">
               <label for="rbtRecover"><a class="MouesoverPopup" >Recover</a> from an emergency event? 
                      <asp:RequiredFieldValidator ID="reqRecover" runat="server" 
                             ValidationGroup="Save" ControlToValidate="rbtRecover"
                             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
                <asp:RadioButtonList ID="rbtRecover" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        
                </asp:RadioButtonList>
                <label class="defined" for="rbtRecover">Recovery is the capabilities necessary to assist schools affected by an event or emergency in restoring the learning environment. For example, providing stress management, such as group discussions, during class time.</label>
              
              </div><!-- /questionGroup -->  
                
			
          </fieldset>

<fieldset class="assessForm multiPart">
  <legend>6. Your school may already employ strategies for responding to a variety of threats or hazards. For example, when you practice regular drills, such as fire drills, you are practicing 
      ways to respond to specific events. Does your school currently practice any of the following drills? </legend> 
      
			<div class="questionGroup">
                   <label for="rbtDenyEntry"><a class="MouesoverPopup" >Lockdown:</a><i> Secure school buildings and grounds during incidents that pose an immediate threat or violence in and around the school.</i>
                        <asp:RequiredFieldValidator ID="reqDenyEntry" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtDenyEntry"
                                 ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                   </label>
                    <asp:RadioButtonList ID="rbtDenyEntry" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow" >
            
                    </asp:RadioButtonList>
			</div><!-- /questionGroup -->

			<div class="questionGroup">
                   <label for="rbtEvacuation"><a class="MouesoverPopup" >Evacuation:</a> <i>Evacuate school buildings and grounds. </i> 
                        <asp:RequiredFieldValidator ID="reqEvacuation" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtEvacuation"
                                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                   </label>
                    <asp:RadioButtonList ID="rbtEvacuation" runat="server" CssClass="leftAlign"  RepeatDirection="Vertical" RepeatLayout="Flow">
            
                    </asp:RadioButtonList>
			</div><!-- /questionGroup -->

			<div class="questionGroup">
                   <label for="rbtShelterInPlace"><a class="MouesoverPopup" >Shelter-in-place:</a> <i>Students and staff remain indoors, perhaps for an extended period of time, because it is safer inside the building or a room than outside.  </i>
                        <asp:RequiredFieldValidator ID="reqShelterInPlace" runat="server" 
                                 ValidationGroup="Save" ControlToValidate="rbtShelterInPlace"
                                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            
                   </label>
                    <asp:RadioButtonList ID="rbtShelterInPlace" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            
                    </asp:RadioButtonList>
			</div><!-- /questionGroup -->
  </fieldset>
    <div class="prevNext">
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click"             />
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />
       
    <%--</div>
       <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">4/11</p></div>--%>
  
        <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" ValidationGroup="Save" runat="server" ForeColor="Red" 
             HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

 
   <div id='PopUp' class="PopUp"><span class="b-close"></span>
              <span>
                  <ol type="i">
                   <li>Prevention: The action schools take to prevent a threatened or actual incident from occurring. For example, establishing access control procedures and providing IDs for students and staff might prevent a dangerous intruder from coming onto school grounds.</li>
                  <li>Protection: Protection focuses on ongoing actions that protect students, teachers, staff, visitors, networks, and property from a threat or hazard. For example, conducting hurricane drills can reduce injury to students and staff because they will know what to do to avoid harm.</li>
                  <li>Mitigation: The capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the impact of an event or emergency. For example, you may mitigate the impact of a possible earthquake by securing book-cases.</li>
                   <li>Response: The capabilities necessary to stabilize an emergency once it has already happened or is certain to happen in an unpreventable way. For example, evacuating or locking down the school, as appropriate.</li>
                  <li>Recovery: The capabilities necessary to assist schools affected by an event or emergency in restoring the learning environment. For example, providing stress management, such as group discussions, during class time.</li>             
                   </ol>
              </span>
            </div> 
    <div id='PopUp2' class="PopUp"><span class="b-close"></span>
              <span>
                  <ol type="i">
                   <li>Lockdown: Secure school buildings and grounds during incidents that pose an immediate threat or violence in and around the school.</li>
                  <li>Evacuation: Evacuate school buildings and grounds.</li>
                  <li>Shelter-in-place: Students and staff remain indoors, perhaps for an extended period of time, because it is safer inside the building or a room than outside.</li>
                    </ol>
              </span>
            </div>
    
    <div id='PopUp3' class="PopUp" ><span class="b-close"></span>
              <span>
                 Threats are human-caused emergencies such as fire, gang violence, cyber-attacks, and <i>active shooters</i>.
              </span>
    </div>
    <div id='PopUp4' class="PopUp" ><span class="b-close"></span>
              <span>
                 Hazards may be natural disasters, such as earthquakes, wildfires, and floods, technological, such as hazardous materials releases, and biological, such as infectious disease outbreaks.
                
              </span>
    </div>
     <div id='PopUp5' class="PopUp" ><span class="b-close"></span>
              <span>
                Prevention: The action schools take to prevent a threatened or actual incident from occurring. For example, establishing access control procedures
                   and providing IDs for students and staff might prevent a dangerous intruder from coming onto school grounds.
                 
              </span>
    </div>
     <div id='PopUp6' class="PopUp" ><span class="b-close"></span>
              <span>
                Protection: Protection focuses on ongoing actions that protect students, teachers, staff, visitors, networks, and property from a threat or hazard. For example,
                   conducting hurricane drills can reduce injury to students and staff because they will know what to do to avoid harm.
              </span>
    </div>
     <div id='PopUp7' class="PopUp" ><span class="b-close"></span>
              <span>
                 Mitigation: The capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the 
                  impact of an event or emergency. For example, you may mitigate the impact of a possible earthquake by securing book-cases.
                   
              </span>
    </div>
     <div id='PopUp8' class="PopUp" ><span class="b-close"></span>
              <span>
                 Response: The capabilities necessary to stabilize an emergency once it has already happened or is certain to 
                  happen in an unpreventable way. For example, evacuating or locking down the school, as appropriate.
              </span>
    </div>
     <div id='PopUp9' class="PopUp" ><span class="b-close"></span>
              <span>
               
                  Recovery: The capabilities necessary to assist schools affected by an event or emergency in restoring the 
                  learning environment. For example, providing stress management, such as group discussions, during class time.             
                    
              </span>
    </div>
     <div id='PopUp10' class="PopUp" ><span class="b-close"></span>
              <span>
                Lockdown: Secure school buildings and grounds during incidents that pose an immediate threat or violence in and around the school.
                
              </span>
    </div>
     <div id='PopUp11' class="PopUp" ><span class="b-close"></span>
              <span>
                  Evacuation: Evacuate school buildings and grounds.
                
              </span>
    </div>
     <div id='PopUp12' class="PopUp" ><span class="b-close"></span>
              <span>
                Shelter-in-place: Students and staff remain indoors, perhaps for an extended period of time, because it is safer 
                  inside the building or a room than outside.
                    
              </span>
    </div>  

</asp:Content>


