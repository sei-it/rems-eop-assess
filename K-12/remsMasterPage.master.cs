﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class remsMasterPage : System.Web.UI.MasterPage
{
    int lngPkID;
    string sPage, sPage1, sPage2, sPage3, sPage4, sPage5, sPage6, sPage7, sPage8, sPage9, sPage10;
    string sHPage, sHPage1, sHPage2, sHPage3, sHPage4, sHPage5, sHPage6, sHPage7, sHPage8, sHPage9, sHPage10;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.QueryString["lngPkID"] != null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            Navigation_Tabs();  
        }

        if (Session["Pagetext"] != null)
        {
            sPage = Session["Pagetext"].ToString();
            pageno.Text = sPage;
        }

        //Navigation_Tabs();  

    }

    //-----K12 or IHE Selected ------//
    private void Navigation_Tabs()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oNav = from css in db.IHE_K12s
                       where css.K12_IHE_Id == lngPkID
                       select css;

            foreach (var nav in oNav)
            {
                if (nav.K12_IHE_Selected == 1)  // for k12
                {
                    GetK12_strNavagiation();
                    K12stepsNav();


                    // for IHE Active
                    lb_IHEDemographic.Visible = false;
                    lbIHE_Intro.Visible = false;
                    lb_IHEThreatHarzards.Visible = false;
                    lb_IHEAccountingComm.Visible = false;
                    lb_IHEPlanningEduComm.Visible = false;
                    lb_IHELawregulations.Visible = false;
                    lb_IHETrainingDoc.Visible = false;
                    lb_IHERecovery.Visible = false;
                    lb_IHEOtherConsideration.Visible = false;
                    lb_IHEConclusion.Visible = false;

                    // For K12 active
                    lbDemographic.Visible = true;
                    lbIntroduction.Visible = true;
                    lbThreatHazards.Visible = true;
                    lbAccountingComm.Visible = true;
                    lbPlanningEduComm.Visible = true;
                    lbLawregulations.Visible = true;
                    lbTrainingDoc.Visible = true;
                    lbRecovery.Visible = true;
                    lbOtherConsideration.Visible = true;
                    lbConclusion.Visible = true;
                }
                else if (nav.K12_IHE_Selected == 2) // for IHE
                {
                    GetIHE_strNavagiation();
                    IHEstepsNav();

                    // for IHE Active
                    lb_IHEDemographic.Visible = true;
                    lbIHE_Intro.Visible = true;
                    lb_IHEThreatHarzards.Visible = true;
                    lb_IHEAccountingComm.Visible = true;
                    lb_IHEPlanningEduComm.Visible = true;
                    lb_IHELawregulations.Visible = true;
                    lb_IHETrainingDoc.Visible = true;
                    lb_IHERecovery.Visible = true;
                    lb_IHEOtherConsideration.Visible = true;
                    lb_IHEConclusion.Visible = true;

                    // For K12 active
                    lbDemographic.Visible = false;
                    lbIntroduction.Visible = false;
                    lbThreatHazards.Visible = false;
                    lbAccountingComm.Visible = false;
                    lbPlanningEduComm.Visible = false;
                    lbLawregulations.Visible = false;
                    lbTrainingDoc.Visible = false;
                    lbRecovery.Visible = false;
                    lbOtherConsideration.Visible = false;
                    lbConclusion.Visible = false;
                }
            }
        }
        
    }

    //If k12 then-----//
    private void GetK12_strNavagiation()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {

            var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                        where css.IHE_K12_FK == lngPkID
                        select css;

            foreach (var oRes in oRess)
            {


                // For Welcome page

                if ((oRes.DEMO_AK12_ORGANIZATION_FROM_FK != null) && (oRes.DEMO_BK12_YOUR_ROLE_FK != null))
                {
                    sPage1 = "true";
                }
                else
                {
                    sPage1 = "false";
                }

                //For Page Collaborative Planning
                if ((oRes.INTRO_AK12_EMERGENCY_PLAN_FK != null) && (oRes.INTRO_BK12_INDIVIDUAL_OR_TEAM_SAFETY_FK != null))
                {
                    if ((oRes.INTRO_C1K12_FIRST_RESPONDERS_FK != null)
                        && (oRes.INTRO_C2K12_LOCAL_EMERGENCY_MNG_STAFF_FK != null)
                        && (oRes.INTRO_C3K12_LOCAL_LAW_ENFORCEMENT_FK != null)
                        && (oRes.INTRO_C4K12_EMS_PERSONNEL_FK != null)
                        && (oRes.INTRO_C5K12_SCHOOL_RESOURCE_OFF_FK != null)
                        && (oRes.INTRO_C6K12_FIRE_OFFICALS_FK != null)
                        && (oRes.INTRO_C7K12_PUBLIC_MENTAL_PRACTITIONERS_FK != null)
                        )
                    {
                        sPage2 = "true";
                    }
                    else
                    {
                        sPage2 = "false";
                    }
                }
                else
                {
                    sPage2 = "false";
                }

                //For page Threats and Hazards
                if ((oRes.THREAT_HAZARDS_AK12_SCHOOL_LIST_OFRISKS_FK != null)
                    && (oRes.THREAT_HAZARDS_B1K12_PREVENT_FK != null)
                    && (oRes.THREAT_HAZARDS_B2K12_PROTECT_FK != null)
                    && (oRes.THREAT_HAZARDS_B3K12_MITIGATE_FK != null)
                    && (oRes.THREAT_HAZARDS_B4K12_RESPOND_FK != null)
                    && (oRes.THREAT_HAZARDS_B5K12_RECOVER_FK != null)
                    && (oRes.THREAT_HAZARDS_C1K12_LOCKDOWN_FK != null)
                    && (oRes.THREAT_HAZARDS_C2K12_EVACUATION_FK != null)
                    && (oRes.THREAT_HAZARDS_C3K12_SHELTER_IN_PLACE_FK != null))
                {
                    sPage3 = "true";
                }
                else
                {
                    sPage3 = "false";
                }

                // for Page Accounting for all Persons and Communications and Warning
                if ((oRes.ACCOUNT_COMM_A1K12_BEFORE_EMG_EVENT_FK != null)
                      && (oRes.ACCOUNT_COMM_A2K12_DURING_EMG_EVENT_FK != null)
                      && (oRes.ACCOUNT_COMM_A3K12_AFTER_EMG_EVENT_FK != null)
                      && (oRes.ACCOUNT_COMM_B1K12_PRO_TOFOLLOW_FK != null)
                      && (oRes.ACCOUNT_COMM_B2K12_COMM_PRESS_MEDIA_FK != null)
                   )
                {
                    sPage4 = "true";
                }
                else
                {
                    sPage4 = "false";
                }

                // For Planning for the Whole School Community  

                if ((oRes.PLANNING_EDU_COMM_AK12_ADDRESS_NEEDS_FK != null)
                   && (oRes.PLANNING_EDU_COMM_B1K12_DIFF_LOC_BESIDES_SCH_FK != null)
                   && (oRes.PLANNING_EDU_COMM_B2K12_AT_OFF_LOCALES_FK != null)
                   && (oRes.PLANNING_EDU_COMM_B3K12_DURING_ALT_TIMES_FK != null)
                   && (oRes.PLANNING_EDU_COMM_CK12_TRAINED_GROUPS_FK != null)
                    )
                {
                    sPage5 = "true";
                }
                else
                {
                    sPage5 = "false";
                }

                //For Laws and Regulations
                if ((oRes.LAW_REG_AK12_AWARE_LOCAL_STATE_FEDERAL_LAW_FK != null)
                    && (oRes.LAW_REG_BK12_HAVE_EMERGENCY_PLAN_FK != null))
                {
                    sPage6 = "true";
                }
                else
                {
                    sPage6 = "false";
                }

                // for Training and Documentation
                if ((oRes.TRAINING_DOC_AK12_SCHL_ACCESSIBLE_TOOL_HANDY_FK != null)
                  && (oRes.TRAINING_DOC_BK12_SCHOOL_POST_IMP_INFO_FK != null)
                  && (oRes.TRAINING_DOC_CK12_ADA_GUIDANCE_FK != null))
                {
                    sPage7 = "true";
                }
                else
                {
                    sPage7 = "false";
                }

                //Continuity of Operations and Recovery

                if ((oRes.RECOVERY_A1K12_CON_BUSINESS_OPERATIONS_FK != null)
                  && (oRes.RECOVERY_A2K12_CON_TEACHING_LEARNING_FK != null)
                  && (oRes.RECOVERY_A3K12_HELP_STUDENTS_STAFF_FK != null)
                  && (oRes.RECOVERY_BK12_SCHOOL_CONSIDERED_STRATEGIES_FK != null))
                {
                    sPage8 = "true";
                }
                else
                {
                    sPage8 = "false";
                }
                //Other Considerations

                //
                if ((oRes.OTHER_CONSIDERATIONS_AK12_APPRO_PERSONNEL_FK != null)
                    && (oRes.OTHER_CONSIDERATIONS_BK12_AWARE_EMG_RESPONSE_FK != null))
                {
                    sPage9 = "true";
                }
                else
                {
                    sPage9 = "false";
                }


            }
        }
    }
    private void K12stepsNav()
    {
        if (sPage1 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = false;
            lbAccountingComm.Enabled = false;
            lbPlanningEduComm.Enabled = false;
            lbLawregulations.Enabled = false;
            lbTrainingDoc.Enabled = false;
            lbRecovery.Enabled = false;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }

        if (sPage2 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = false;
            lbPlanningEduComm.Enabled = false;
            lbLawregulations.Enabled = false;
            lbTrainingDoc.Enabled = false;
            lbRecovery.Enabled = false;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }
        if (sPage3 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = false;
            lbLawregulations.Enabled = false;
            lbTrainingDoc.Enabled = false;
            lbRecovery.Enabled = false;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }
        if (sPage4 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = true;
            lbLawregulations.Enabled = false;
            lbTrainingDoc.Enabled = false;
            lbRecovery.Enabled = false;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }
        if (sPage5 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = true;
            lbLawregulations.Enabled = true;
            lbTrainingDoc.Enabled = false;
            lbRecovery.Enabled = false;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }
        if (sPage6 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = true;
            lbLawregulations.Enabled = true;
            lbTrainingDoc.Enabled = true;
            lbRecovery.Enabled = false;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }
        if (sPage7 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = true;
            lbLawregulations.Enabled = true;
            lbTrainingDoc.Enabled = true;
            lbRecovery.Enabled = true;
            lbOtherConsideration.Enabled = false;
            lbConclusion.Enabled = false;
        }
        if (sPage8 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = true;
            lbLawregulations.Enabled = true;
            lbTrainingDoc.Enabled = true;
            lbRecovery.Enabled = true;
            lbOtherConsideration.Enabled = true;
            lbConclusion.Enabled = false;
        }
        if (sPage9 == "true")
        {
            lbIntroduction.Enabled = true;
            lbThreatHazards.Enabled = true;
            lbAccountingComm.Enabled = true;
            lbPlanningEduComm.Enabled = true;
            lbLawregulations.Enabled = true;
            lbTrainingDoc.Enabled = true;
            lbRecovery.Enabled = true;
            lbOtherConsideration.Enabled = true;
            lbConclusion.Enabled = true;
        }


    }    

    //If IHE then-----//
    private void GetIHE_strNavagiation()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {


            var oRihe = from iHe in db.IHE_AssessmentQuestions
                        where iHe.IHE_K12_FK == lngPkID
                        select iHe;
            foreach (var oRes in oRihe)
            {
                // For Welcome page

                if ((oRes.DEMO_A_ORGANIZATION_FROM_FK != null) && (oRes.DEMO_B_YOUR_ROLE_FK != null))
                {
                    sHPage1 = "true";
                }
                else
                {
                    sHPage1 = "false";
                }

                //For Page Collaborative Planning
                if ((oRes.INTRO_A_EMERGENCY_PLAN_FK != null) && (oRes.INTRO_B_INDIVIDUAL_OR_TEAM_SAFETY_FK != null))
                {
                    if ((oRes.INTRO_C1_FIRST_RESPONDERS_FK != null)
                        && (oRes.INTRO_C2_LOCAL_EMERGENCY_MNG_FK != null)
                        && (oRes.INTRO_C3_LOCAL_LAW_ENFORCEMENT_FK != null)
                        && (oRes.INTRO_C4_EMS_PERSONNEL_FK != null)                       
                        && (oRes.INTRO_C5_FIRE_OFFICALS_FK != null)
                        && (oRes.INTRO_C6_PUBLIC_MENTAL_PRACTITIONERS_FK != null)
                        )
                    {
                        sHPage2 = "true";
                    }
                    else
                    {
                        sHPage2 = "false";
                    }
                }
                else
                {
                    sHPage2 = "false";
                }

                //For page Threats and Hazards
                if ((oRes.THREAT_HAZARDS_A_IHE_PLANNING_THOUGHT_FK != null)
                    && (oRes.THREAT_HAZARDS_B1_PREVENT_FK != null)
                    && (oRes.THREAT_HAZARDS_B2_PROTECT_FK != null)
                    && (oRes.THREAT_HAZARDS_B3_MITIGATE_FK != null)
                    && (oRes.THREAT_HAZARDS_B4_RESPOND_FK != null)
                    && (oRes.THREAT_HAZARDS_B5_RECOVER_FK != null)
                    && (oRes.THREAT_HAZARDS_C1_DENY_CLOSING_FK != null)
                    && (oRes.THREAT_HAZARDS_C2_EVACUATION_FK != null)
                    && (oRes.THREAT_HAZARDS_C3_SHELTER_IN_PLACE_FK != null))
                {
                    sHPage3 = "true";
                }
                else
                {
                    sHPage3 = "false";
                }

                // for Page Accounting for all Persons and Communications and Warning
                if ((oRes.ACCOUNT_COMM_A1_BEFORE_EMG_EVENT_FK != null)
                      && (oRes.ACCOUNT_COMM_A2_DURING_EMG_EVENT_FK != null)
                      && (oRes.ACCOUNT_COMM_A3_AFTER_EMG_EVENT_FK != null)
                      && (oRes.ACCOUNT_COMM_B1_ANNOUNCING_PRO_TOFOLLOW_FK != null)
                      && (oRes.ACCOUNT_COMM_B2_COMMUNICATING_PRESS_MEDIA_FK != null)
                   )
                {
                    sHPage4 = "true";
                }
                else
                {
                    sHPage4 = "false";
                }

                // For Planning for the Whole School Community  

                if ((oRes.PLANNING_EDU_COMM_A_HOW_YOULL_ADDRESS_FK != null)
                   && (oRes.PLANNING_EDU_COMM_B1_DIFF_LOC_BESIDES_MAIN_FK != null)
                   && (oRes.PLANNING_EDU_COMM_B2_AT_OFF_CAMPUS_FK != null)
                   && (oRes.PLANNING_EDU_COMM_B3_DURING_ALTERNATE_TIMES_FK != null)
                   && (oRes.PLANNING_EDU_COMM_C_TRAINED_GROUPS_FK != null)
                    )
                {
                    sHPage5 = "true";
                }
                else
                {
                    sHPage5 = "false";
                }

                //For Laws and Regulations
                if ((oRes.LAW_REGULATIONS_A_AWARE_LOCAL_STATE_FEDERAL_LAW_FK != null)
                    && (oRes.LAW_REGULATIONS_B_HAVE_EMERGENCY_PLAN_FK != null))
                {
                    sHPage6 = "true";
                }
                else
                {
                    sHPage6 = "false";
                }

                // for Training and Documentation
                if ((oRes.TRAINING_DOC_A_SCHOOL_ACCESSIBLE_TOOL_HANDY_FK != null)
                  && (oRes.TRAINING_DOC_B_SCHOOL_POST_IMP_INFO_FK != null)
                  && (oRes.TRAINING_DOC_C_ADA_GUIDANCE_FK != null))
                {
                    sHPage7 = "true";
                }
                else
                {
                    sHPage7 = "false";
                }

                //Continuity of Operations and Recovery

                if ((oRes.RECOVERY_A1_CON_BUSINESS_OPERATIONS_FK != null)
                  && (oRes.RECOVERY_A2_CON_TEACHING_LEARNING_FK != null)
                  && (oRes.RECOVERY_A3_HELP_STUDENTS_STAFF_FK != null)
                  && (oRes.RECOVERY_B_SCHOOL_CONSIDERED_STRATEGIES_FK != null))
                {
                    sHPage8 = "true";
                }
                else
                {
                    sHPage8 = "false";
                }
                //Other Considerations

                //
                if ((oRes.OTHER_CONSIDERATIONS_A_APPRO_PERSONNEL_FK != null)
                    && (oRes.OTHER_CONSIDERATIONS_B_AWARE_EMG_RESPONSE_FK != null))
                {
                    sHPage9 = "true";
                }
                else
                {
                    sHPage9 = "false";
                }
            }
        }
    }
    private void IHEstepsNav()
    {

        if (sHPage1 == "true")
        {
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = false;
            lb_IHEAccountingComm.Enabled = false;
            lb_IHEPlanningEduComm.Enabled = false;
            lb_IHELawregulations.Enabled = false;
            lb_IHETrainingDoc.Enabled = false;
            lb_IHERecovery.Enabled = false;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }

        if (sHPage2 == "true")
        {
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = false;
            lb_IHEPlanningEduComm.Enabled = false;
            lb_IHELawregulations.Enabled = false;
            lb_IHETrainingDoc.Enabled = false;
            lb_IHERecovery.Enabled = false;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage3 == "true")
        {

            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = false;
            lb_IHELawregulations.Enabled = false;
            lb_IHETrainingDoc.Enabled = false;
            lb_IHERecovery.Enabled = false;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage4 == "true")
        {
           
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = true;
            lb_IHELawregulations.Enabled = false;
            lb_IHETrainingDoc.Enabled = false;
            lb_IHERecovery.Enabled = false;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage5 == "true")
        {
       
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = true;
            lb_IHELawregulations.Enabled = true;
            lb_IHETrainingDoc.Enabled = false;
            lb_IHERecovery.Enabled = false;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage6 == "true")
        {
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = true;
            lb_IHELawregulations.Enabled = true;
            lb_IHETrainingDoc.Enabled = true;
            lb_IHERecovery.Enabled = false;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage7 == "true")
        {
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = true;
            lb_IHELawregulations.Enabled = true;
            lb_IHETrainingDoc.Enabled = true;
            lb_IHERecovery.Enabled = true;
            lb_IHEOtherConsideration.Enabled = false;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage8 == "true")
        {
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = true;
            lb_IHELawregulations.Enabled = true;
            lb_IHETrainingDoc.Enabled = true;
            lb_IHERecovery.Enabled = true;
            lb_IHEOtherConsideration.Enabled = true;
            lb_IHEConclusion.Enabled = false;
        }
        if (sHPage9 == "true")
        {
            lbIHE_Intro.Enabled = true;
            lb_IHEThreatHarzards.Enabled = true;
            lb_IHEAccountingComm.Enabled = true;
            lb_IHEPlanningEduComm.Enabled = true;
            lb_IHELawregulations.Enabled = true;
            lb_IHETrainingDoc.Enabled = true;
            lb_IHERecovery.Enabled = true;
            lb_IHEOtherConsideration.Enabled = true;
            lb_IHEConclusion.Enabled = true;
        }

    }



    //protected void btnBack_Click(object sender, EventArgs e)
    //{      

    //}
    //protected void btnNext_Click(object sender, EventArgs e)
    //{

    //    if (sPage == "1/10")
    //    {
    //        Response.Redirect("B_Introduction.aspx?lngPkID=" + lngPkID);
    //    }

    //}

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    // for K12
    protected void lbDemographic_Click(object sender, EventArgs e)
    {
        if (sPage1 == "true")
        {

            Response.Redirect("A_Demographic.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbIntroduction_Click(object sender, EventArgs e)
    {
        if (sPage1 == "true")
        {

            Response.Redirect("B_Introduction.aspx?lngPkID=" + lngPkID);
        }

    }
    protected void lbThreatHazards_Click(object sender, EventArgs e)
    {
        if (sPage2 == "true")
        {

            Response.Redirect("C_ThreatHazards.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbAccountingComm_Click(object sender, EventArgs e)
    {
        if (sPage3 == "true")
        {

            Response.Redirect("D_AccountingCommunication.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbPlanningEduComm_Click(object sender, EventArgs e)
    {
        if (sPage4 == "true")
        {

            Response.Redirect("E_PlanningEduCommunity.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbLawregulations_Click(object sender, EventArgs e)
    {
        if (sPage5 == "true")
        {

            Response.Redirect("F_LawsRegulations.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbTrainingDoc_Click(object sender, EventArgs e)
    {
        if (sPage6 == "true")
        {

            Response.Redirect("G_TrainingDocumentation.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbRecovery_Click(object sender, EventArgs e)
    {
        if (sPage7 == "true")
        {

            Response.Redirect("H_Recovery.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbOtherConsideration_Click(object sender, EventArgs e)
    {
        if (sPage8 == "true")
        {

            Response.Redirect("I_OtherConsideration.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lbConclusion_Click(object sender, EventArgs e)
    {
        if (sPage9 == "true")
        {

            Response.Redirect("J_Conclusion.aspx?lngPkID=" + lngPkID);
        }
    }

    // for IHE
    protected void lb_IHEDemographic_Click(object sender, EventArgs e)
    {
        if (sHPage1 == "true")
        {
            Response.Redirect("A_Demographic.aspx?lngPkID=" + lngPkID);
        }
    }   
    protected void lbIHE_Intro_Click(object sender, EventArgs e)
    {
        if (sHPage1 == "true")
        {

            Response.Redirect("B_IHEIntroduction.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHEHarzards_Click(object sender, EventArgs e)
    {
        if (sHPage2 == "true")
        {

            Response.Redirect("C_IHEThreatHazards.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHEAccountingComm_Click(object sender, EventArgs e)
    {
        if (sHPage3 == "true")
        {

            Response.Redirect("D_IHEAccountingCommunication.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHEPlanningEduComm_Click(object sender, EventArgs e)
    {
        if (sHPage4 == "true")
        {

            Response.Redirect("E_IHEPlanningEduCommunity.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHELawregulations_Click(object sender, EventArgs e)
    {
        if (sHPage5 == "true")
        {

            Response.Redirect("F_IHELawsRegulations.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHETrainingDoc_Click(object sender, EventArgs e)
    {
        if (sHPage6 == "true")
        {

            Response.Redirect("G_IHETrainingDocumentation.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHERecovery_Click(object sender, EventArgs e)
    {
        if (sHPage7 == "true")
        {

            Response.Redirect("H_IHERecovery.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void lb_IHEOtherConsideration_Click(object sender, EventArgs e)
    {
        if (sHPage8 == "true")
        {

            Response.Redirect("I_IHEOtherConsideration.aspx?lngPkID=" + lngPkID);
        }

    }
    protected void lb_IHEConclusion_Click(object sender, EventArgs e)
    {
        if (sHPage9 == "true")
        {

            Response.Redirect("J_IHEConclusion.aspx?lngPkID=" + lngPkID);
        }
    }

  
}
