﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class E_IHEPlanningEduCommunity : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "5/10";
    }



    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.PLANNING_EDU_COMM_A_HOW_YOULL_ADDRESS_FK;
                if (objVal != null)
                {
                    rbtAddressNeedsWithDisab.SelectedIndex = rbtAddressNeedsWithDisab.Items.IndexOf(rbtAddressNeedsWithDisab.Items.FindByValue(oRes.PLANNING_EDU_COMM_A_HOW_YOULL_ADDRESS_FK.ToString()));
                }
                objVal = oRes.PLANNING_EDU_COMM_B1_DIFF_LOC_BESIDES_MAIN_FK;
                if (objVal != null)
                {
                    rbtDiffLocation.SelectedIndex = rbtDiffLocation.Items.IndexOf(rbtDiffLocation.Items.FindByValue(oRes.PLANNING_EDU_COMM_B1_DIFF_LOC_BESIDES_MAIN_FK.ToString()));
                }
                objVal = oRes.PLANNING_EDU_COMM_B2_AT_OFF_CAMPUS_FK;
                if (objVal != null)
                {
                    rbtOffCampus.SelectedIndex = rbtOffCampus.Items.IndexOf(rbtOffCampus.Items.FindByValue(oRes.PLANNING_EDU_COMM_B2_AT_OFF_CAMPUS_FK.ToString()));
                }
                objVal = oRes.PLANNING_EDU_COMM_B3_DURING_ALTERNATE_TIMES_FK;
                if (objVal != null)
                {
                    rbtAlternateTimes.SelectedIndex = rbtAlternateTimes.Items.IndexOf(rbtAlternateTimes.Items.FindByValue(oRes.PLANNING_EDU_COMM_B3_DURING_ALTERNATE_TIMES_FK.ToString()));
                }
                objVal = oRes.PLANNING_EDU_COMM_C_TRAINED_GROUPS_FK;
                if (objVal != null)
                {
                    rbtTrainedGroup.SelectedIndex = rbtTrainedGroup.Items.IndexOf(rbtTrainedGroup.Items.FindByValue(oRes.PLANNING_EDU_COMM_C_TRAINED_GROUPS_FK.ToString()));
                }
            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.IHE_K12_FK == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;
            }

            if (!(rbtAddressNeedsWithDisab.SelectedItem == null))
            {
                if (!(rbtAddressNeedsWithDisab.SelectedItem.Value.ToString() == ""))
                {
                    oRes.PLANNING_EDU_COMM_A_HOW_YOULL_ADDRESS_FK = Convert.ToInt32(rbtAddressNeedsWithDisab.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.PLANNING_EDU_COMM_A_HOW_YOULL_ADDRESS_FK = null;
                }
            }
            if (!(rbtDiffLocation.SelectedItem == null))
            {
                if (!(rbtDiffLocation.SelectedItem.Value.ToString() == ""))
                {
                    oRes.PLANNING_EDU_COMM_B1_DIFF_LOC_BESIDES_MAIN_FK = Convert.ToInt32(rbtDiffLocation.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.PLANNING_EDU_COMM_B1_DIFF_LOC_BESIDES_MAIN_FK = null;
                }
            }
            if (!(rbtOffCampus.SelectedItem == null))
            {
                if (!(rbtOffCampus.SelectedItem.Value.ToString() == ""))
                {
                    oRes.PLANNING_EDU_COMM_B2_AT_OFF_CAMPUS_FK = Convert.ToInt32(rbtOffCampus.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.PLANNING_EDU_COMM_B2_AT_OFF_CAMPUS_FK = null;
                }
            }
            if (!(rbtAlternateTimes.SelectedItem == null))
            {
                if (!(rbtAlternateTimes.SelectedItem.Value.ToString() == ""))
                {
                    oRes.PLANNING_EDU_COMM_B3_DURING_ALTERNATE_TIMES_FK = Convert.ToInt32(rbtAlternateTimes.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.PLANNING_EDU_COMM_B3_DURING_ALTERNATE_TIMES_FK = null;
                }
            }
            if (!(rbtTrainedGroup.SelectedItem == null))
            {
                if (!(rbtTrainedGroup.SelectedItem.Value.ToString() == ""))
                {
                    oRes.PLANNING_EDU_COMM_C_TRAINED_GROUPS_FK = Convert.ToInt32(rbtTrainedGroup.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.PLANNING_EDU_COMM_C_TRAINED_GROUPS_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.YesNo_Nots
                      select e;

            rbtAddressNeedsWithDisab.DataSource = qry;
            rbtAddressNeedsWithDisab.DataTextField = "Yes_No_Not";
            rbtAddressNeedsWithDisab.DataValueField = "NotID";
            rbtAddressNeedsWithDisab.DataBind();

            rbtDiffLocation.DataSource = qry;
            rbtDiffLocation.DataTextField = "Yes_No_Not";
            rbtDiffLocation.DataValueField = "NotID";
            rbtDiffLocation.DataBind();

            rbtOffCampus.DataSource = qry;
            rbtOffCampus.DataTextField = "Yes_No_Not";
            rbtOffCampus.DataValueField = "NotID";
            rbtOffCampus.DataBind();

            rbtAlternateTimes.DataSource = qry;
            rbtAlternateTimes.DataTextField = "Yes_No_Not";
            rbtAlternateTimes.DataValueField = "NotID";
            rbtAlternateTimes.DataBind();

            rbtTrainedGroup.DataSource = qry;
            rbtTrainedGroup.DataTextField = "Yes_No_Not";
            rbtTrainedGroup.DataValueField = "NotID";
            rbtTrainedGroup.DataBind();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }


    protected void btnBackIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("D_IHEAccountingCommunication.aspx?lngPkID=" + lngPkID);
    }
    protected void btnNextIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("F_IHELawsRegulations.aspx?lngPkID=" + lngPkID);
    }
}