﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="A_Demographic.aspx.cs" Inherits="A_Demographic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" runat="Server">

    <fieldset>
        <div id="active1"></div>
        <!-- /active1 -->
        <h1>Welcome to EOP ASSESS!</h1>

        <p>This tool is designed to help you assess your capacity to create and maintain a high-quality school or institution of higher education emergency operations plan (EOP) that considers a range of threats and hazards. The tool contains questions that will assess your current understanding of your organization’s plan, and help identify areas for learning opportunities, while also taking you through a planning process recommended by the U.S. Departments of Education, Homeland Security, Justice, and Health and Human Services. Upon completing the self-assessment, your responses will result in a customized downloadable report containing a variety of additional resources and information relevant to your needs. This tool should take approximately 12-15 minutes to complete. Please note that no information is recorded during this process; your responses are used only to generate your customized report, and no data remains in our system upon completion and download of the report.
<p>For assistance using this tool, please contact the Readiness and Emergency Management for Schools (REMS) Technical Assistance (TA) Center Help Desk by email <a href="mailto:info@remstacenter.org">(info@remstacenter.org)</a> or by telephone (855-781-7367 [REMS]), toll-free Monday through Friday between the hours of 9:00 a.m. and 5:00 p.m. Eastern Time.</p>
<p>Let’s get started! Please note that all fields are required. Select Next to continue.</p>
</p>
        <%--<p>
            This tool is designed to help you assess your capacity to create and 
         maintain a high-quality school or institution of higher education emergency 
         operations plan that considers a range of threats and hazards. The tool 
         contains questions that will assess your current understanding of your 
         organization’s plan, and help identify areas for learning opportunities, 
         while also taking you through a planning process recommended by the U.S. 
         Departments of Education, Homeland Security, Justice, and Health and Human 
         Services. Upon completing the self-assessment, your responses will result 
         in a customized downloadable report containing a variety of additional 
         resources and information relevant to your needs. This tool should take 
         approximately 12-15 minutes to complete. Please note that no information 
         is recorded during this process; your responses are used only to generate 
         your customized report, and no data remains in our system upon completion 
         and download of the report.
        </p>
        <p>
            For assistance using this tool, please contact the Readiness and Emergency Management for Schools (REMS) 
    Technical Assistance (TA) Center Help Desk by email <a href="mailto:info@remstacenter.org">(info@remstacenter.org)</a>
            or by telephone (855-781-7367 [REMS]), toll free and between the hours of 9:00 a.m. and 5:00 p.m., Eastern Standard Time.
        </p>
        <p>Let’s get started! Please note that all fields are required. Click NEXT to continue.</p>--%>


    </fieldset>

    <fieldset class="assessForm">
        <legend for="rbtk12OrIHE">Are you affiliated with a K-12 School/District or with an Institution of Higher Education (IHE)?                            
                  
                

            <asp:RequiredFieldValidator ID="reqIHEK12" runat="server"
                ValidationGroup="Save" ControlToValidate="rbtk12OrIHE"
                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

        </legend>

        <asp:RadioButtonList ID="rbtk12OrIHE" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="rbtk12OrIHE_SelectedIndexChanged" RepeatLayout="Flow">
        </asp:RadioButtonList>

    </fieldset>

    <fieldset class="assessForm">
        <legend for="ddlOrganizationFrom">What type of organization are you from?
            <br>
            <em>Please select the option you feel best describes the organization you are considering when taking this assessment.</em>
        </legend>

        <label for="ddlOrganizationFrom">
        </label>
        <p>
            <asp:DropDownList ID="ddlOrganizationFrom" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" Width="175px">
                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
            </asp:DropDownList>
        </p>

        <asp:RequiredFieldValidator ID="reqOrganizationFrom" runat="server" InitialValue="Select"
            ValidationGroup="Save" ControlToValidate="ddlOrganizationFrom"
            ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>



    </fieldset>
    <fieldset class="assessForm">
        <legend for="ddlYourRole">What is your role or function within your organization?</legend>
        <asp:DropDownList ID="ddlYourRole" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" Width="275px">
            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
        </asp:DropDownList>

        <asp:RequiredFieldValidator ID="reqYourRole" runat="server" InitialValue="Select"
            ValidationGroup="Save" ControlToValidate="ddlYourRole"
            ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>


    </fieldset>

    <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" ValidationGroup="Save"
        runat="server" ForeColor="Red"
        HeaderText="All required information was not provided. Please answer the marked question(s)." />

    <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    <div class="prevNext">
        <asp:Button ID="btnBack" runat="server" Visible="false" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server"  ValidationGroup="Save" Text="Next" OnClick="btnNext_Click" />        
        
    </div>

</asp:Content>


