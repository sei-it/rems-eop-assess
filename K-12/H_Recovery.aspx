﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="H_Recovery.aspx.cs" Inherits="H_Recovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" runat="Server">
    <div id="active8"></div>
    <h1>Continuity of Operations and Recovery</h1>


    <fieldset class="assessForm multiPart">

        <legend>17. It is important that your school is able to continue essential services during an emergency and its immediate aftermath in order to normalize operations as quickly as possible. This will enable your school to maintain the safety and well-being of students, staff, and the learning environment in the aftermath of an emergency. Has your school considered strategies for any of the following?</legend>

        <div class="questionGroup">
            <label for="rbtContinuityBussOperations">
                <a class="MouesoverPopup">Continuity of business operations</a>, such as ensuring that teachers are paid, and that your vendors are paid?
            
                    <asp:RequiredFieldValidator ID="reqrContinuityBussOperations" runat="server"
                        ValidationGroup="Save" ControlToValidate="rbtContinuityBussOperations"
                        ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtContinuityBussOperations" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
        </div>
        <!-- /questionGroup -->

        <div class="questionGroup">
            <label for="rbtContinuityTeaching">
                <a class="MouesoverPopup">Continuity of teaching and learning</a>, such as distance learning, using another school’s facilities, and addressing student nutritional needs that depend on school breakfast and lunch programs?   

             

                <asp:RequiredFieldValidator ID="reqContinuityTeaching" runat="server"
                    ValidationGroup="Save" ControlToValidate="rbtContinuityTeaching"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtContinuityTeaching" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
        </div>
        <!-- /questionGroup -->

        <div class="questionGroup">
            <label for="rbtHelpingStudents">
                <a class="MouesoverPopup">Helping students and staff regain a sense of 
           normalcy</a> following a traumatic experience or event?   

           

                <asp:RequiredFieldValidator ID="reqHelpingStudents" runat="server"
                    ValidationGroup="Save" ControlToValidate="rbtHelpingStudents"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtHelpingStudents" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
        </div>
        <!-- /questionGroup -->

    </fieldset>


    <fieldset class="assessForm">

        <legend for="rbtIHEconsiderStrategies">18. Have you or your school considered strategies to facilitate 
           the  <a class="MouesoverPopup">psychological recovery</a>
            of the school community in the aftermath of a traumatic event that impacts your facility?  

           

            <asp:RequiredFieldValidator ID="reqIHEconsiderStrategies" runat="server"
                ValidationGroup="Save" ControlToValidate="rbtIHEconsiderStrategies"
                ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
        </legend>
        <asp:RadioButtonList ID="rbtIHEconsiderStrategies" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

        <label class="noBorder1" for="rbtIHEconsiderStrategies"><i>During and after an emergency, teachers and other staff are a critical link in promoting resilience, in recognizing signs of traumatic stress, and in helping students and their families regain a sense of normalcy. Strategies to facilitate psychological recovery include counseling, peer support programs, afterschool activities, and mental health services.</i></label>

    </fieldset>
    <div class="prevNext">
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />

    </div>
    <%-- <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">9/11</p></div>--%>
    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" CssClass="ValidationSummary" runat="server" ForeColor="Red"
        HeaderText="All required information was not provided. Please answer the marked question(s)." />
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    <div id='PopUp' class="PopUp">
        <span class="b-close"></span>
        <span>During and after an emergency, teachers and other staff are a critical link in promoting 
                  resilience, in recognizing signs of traumatic stress, and in helping students and their 
                  families regain a sense of normalcy. Strategies to facilitate psychological recovery 
                  include counseling, peer support programs, afterschool activities, and mental health services.</span>
    </div>
</asp:Content>


