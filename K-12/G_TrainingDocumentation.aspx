﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="G_TrainingDocumentation.aspx.cs" Inherits="G_TrainingDocumentation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
   <div id="active7"></div> 
<h1>Training and Documentation</h1>
  
 <fieldset class="assessForm">
       <legend for="rbtAccessibleToolsHandy">14. Does your school provide tools and documents that contain instructions for teachers and staff for use during an emergency?

            <asp:RequiredFieldValidator ID="reqAccessibleToolsHandy" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAccessibleToolsHandy"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
       </legend>
        <asp:RadioButtonList ID="rbtAccessibleToolsHandy" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">

        </asp:RadioButtonList>

 </fieldset>
<fieldset class="assessForm">

       <legend for="rbtIHEpostImp">15. Does your school post important information such as exit locations and evacuation routes throughout the building and in plain view for use during an emergency event?

              <asp:RequiredFieldValidator ID="reqIHEpostImp" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtIHEpostImp"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
       </legend>
        <asp:RadioButtonList ID="rbtIHEpostImp" CssClass="leftAlign"  runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

 </fieldset>
  <fieldset class="assessForm">

       <legend for="rbtAccessibleToolComply">16. Do the tools, documents, and posted information mentioned above comply with the requirements put forth in the <a class="MouesoverPopup" ><i>Americans with Disabilities Act (ADA)</i> guidance</a>?

            <asp:RequiredFieldValidator ID="reqAccessibleToolComply" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAccessibleToolComply"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
       </legend>
        <asp:RadioButtonList ID="rbtAccessibleToolComply" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">

        </asp:RadioButtonList>

  </fieldset>


    <div class="prevNext">  
         <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />     
        <asp:Button ID="btnNext" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNext_Click" />
        
       
    </div>
   <%--  <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">8/11</p></div>--%>
        <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" ValidationGroup="Save" runat="server" ForeColor="Red" 
             HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>
</asp:Content>

