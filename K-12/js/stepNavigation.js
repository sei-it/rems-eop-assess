$(document).ready(function() {
	
// Check navigation cookie - add class if cookie exists
	
	function checkNavCookie() {
	if (document.cookie.indexOf("Welcome") >= 0) {
  		$("a#lbDemographic").addClass("visitedNav");
		$("a#lb_IHEDemographic").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Collaboration") >= 0){
		$("a#lbIntroduction").addClass("visitedNav");
		$("a#lbIHE_Intro").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Threats") >= 0){
		$("a#lbThreatHazards").addClass("visitedNav");
		$("a#lb_IHEThreatHarzards").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Accounting") >= 0){
		$("a#lbAccountingComm").addClass("visitedNav");
		$("a#lb_IHEAccountingComm").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Community") >= 0){
		$("a#lbPlanningEduComm").addClass("visitedNav");
		$("a#lb_IHEPlanningEduComm").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Laws") >= 0){
		$("a#lbLawregulations").addClass("visitedNav");
		$("a#lb_IHELawregulations").addClass("visitedNav");
	}
	if (document.cookie.indexOf("Training") >= 0){
		$("a#lbTrainingDoc").addClass("visitedNav");
		$("a#lb_IHETrainingDoc").addClass("visitedNav");
	}
	if (document.cookie.indexOf("COOP") >= 0){
		$("a#lbRecovery").addClass("visitedNav");
		$("a#lb_IHERecovery").addClass("visitedNav");
	}	
	if (document.cookie.indexOf("Considerations") >= 0){
		$("a#lbOtherConsideration").addClass("visitedNav");
		$("a#lb_IHEOtherConsideration").addClass("visitedNav");
	}	
	if (document.cookie.indexOf("Conclusion") >= 0){
		$("a#lbConclusion").addClass("visitedNav");
		$("a#lb_IHEConclusion").addClass("visitedNav");
	}	
}

// Add the selected nav class to current page

		
    if ($("span#pageno").html() == "1/10") {

        $("a#lbDemographic").addClass("selectedNav");
		$("a#lb_IHEDemographic").addClass("selectedNav");
		
    } else {
        $("a#lbDemographic").removeClass("selectedNav");	
		$("a#lb_IHEDemographic").removeClass("selectedNav");	
    }
    if ($("span#pageno").html() == "2/10") {

        $("a#lbIntroduction").addClass("selectedNav");
		$("a#lbIHE_Intro").addClass("selectedNav");
		Cookies.set('visited1', 'Welcome');
	
    } else {
        $("a#lbIntroduction").removeClass("selectedNav");
		$("a#lbIHE_Intro").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageno").html() == "3/10") {

        $("a#lbThreatHazards").addClass("selectedNav");
		$("a#lb_IHEThreatHarzards").addClass("selectedNav");
		Cookies.set('visited2', 'Collaboration');
	
    } else {
        $("a#lbThreatHazards").removeClass("selectedNav");
		$("a#lb_IHEThreatHarzards").removeClass("selectedNav");
		checkNavCookie(); 
    }

    if ($("span#pageno").html() == "4/10") {

        $("a#lbAccountingComm").addClass("selectedNav");
		$("a#lb_IHEAccountingComm").addClass("selectedNav");
		Cookies.set('visited3', 'Threats');
	
    } else {
        $("a#lbAccountingComm").removeClass("selectedNav");
		$("a#lb_IHEAccountingComm").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageno").html() == "5/10") {

        $("a#lbPlanningEduComm").addClass("selectedNav");
		$("a#lb_IHEPlanningEduComm").addClass("selectedNav");
		Cookies.set('visited4', 'Accounting');
	
    } else {
        $("a#lbPlanningEduComm").removeClass("selectedNav");
		$("a#lb_IHEPlanningEduComm").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageno").html() == "6/10") {

        $("a#lbLawregulations").addClass("selectedNav");
		$("a#lb_IHELawregulations").addClass("selectedNav");
		Cookies.set('visited5', 'Community');

    } else {
        $("a#lbLawregulations").removeClass("selectedNav");
		$("a#lb_IHELawregulations").removeClass("selectedNav");
		checkNavCookie();
    }

    if ($("span#pageno").html() == "7/10") {

        $("a#lbTrainingDoc").addClass("selectedNav");
		$("a#lb_IHETrainingDoc").addClass("selectedNav");
		Cookies.set('visited6', 'Laws');
	
    } else {
        $("a#lbTrainingDoc").removeClass("selectedNav");
		$("a#lb_IHETrainingDoc").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageno").html() == "8/10") {

        $("a#lbRecovery").addClass("selectedNav");
		$("a#lb_IHERecovery").addClass("selectedNav");
		Cookies.set('visited7', 'Training');

    } else {
        $("a#lbRecovery").removeClass("selectedNav");
		$("a#lb_IHERecovery").removeClass("selectedNav");
		checkNavCookie();	
    }

    if ($("span#pageno").html() == "9/10") {

        $("a#lbOtherConsideration").addClass("selectedNav");	
		$("a#lb_IHEOtherConsideration").addClass("selectedNav");	
		Cookies.set('visited8', 'COOP');	

    } else {
        $("a#lbOtherConsideration").removeClass("selectedNav");
		$("a#lb_IHEOtherConsideration").removeClass("selectedNav");	
		checkNavCookie();
    }

    if ($("span#pageno").html() == "10/10") {

        $("a#lbConclusion").addClass("selectedNav");
		$("a#lb_IHEConclusion").addClass("selectedNav");
		$("a#lbOtherConsideration").addClass("visitedNav");
		$("a#lb_IHEOtherConsideration").addClass("visitedNav");	
		Cookies.set('visited9', 'Considerations');

    } else {
        $("a#lbConclusion").removeClass("selectedNav");
		$("a#lb_IHEConclusion").removeClass("selectedNav");
		checkNavCookie();	
    }
	
// Delete the navigation cookies when returning to main page


	$( "#listDiv a" ).on( "click", function() {
	  	Cookies.remove('visited1');
		Cookies.remove('visited2');	
		Cookies.remove('visited3');
		Cookies.remove('visited4');
		Cookies.remove('visited5');	
		Cookies.remove('visited6');
		Cookies.remove('visited7');
		Cookies.remove('visited8');	
		Cookies.remove('visited9');	
	});

});