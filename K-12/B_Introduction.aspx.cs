﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class B_Introduction : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "2/10";
    }


    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {

            var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {



                objVal = oRes.INTRO_AK12_EMERGENCY_PLAN_FK;
                if (objVal != null)
                {
                    rbtEmergencyPlan.SelectedIndex = rbtEmergencyPlan.Items.IndexOf(rbtEmergencyPlan.Items.FindByValue(oRes.INTRO_AK12_EMERGENCY_PLAN_FK.ToString()));
                }
                objVal = oRes.INTRO_BK12_INDIVIDUAL_OR_TEAM_SAFETY_FK;
                if (objVal != null)
                {
                    rbtIndividualTeamSafety.SelectedIndex = rbtIndividualTeamSafety.Items.IndexOf(rbtIndividualTeamSafety.Items.FindByValue(oRes.INTRO_BK12_INDIVIDUAL_OR_TEAM_SAFETY_FK.ToString()));
                }
                objVal = oRes.INTRO_C1K12_FIRST_RESPONDERS_FK;
                if (objVal != null)
                {
                    rbtFirstResponders.SelectedIndex = rbtFirstResponders.Items.IndexOf(rbtFirstResponders.Items.FindByValue(oRes.INTRO_C1K12_FIRST_RESPONDERS_FK.ToString()));
                }
                objVal = oRes.INTRO_C2K12_LOCAL_EMERGENCY_MNG_STAFF_FK;
                if (objVal != null)
                {
                    rbtLocalEmergencyStaff.SelectedIndex = rbtLocalEmergencyStaff.Items.IndexOf(rbtLocalEmergencyStaff.Items.FindByValue(oRes.INTRO_C2K12_LOCAL_EMERGENCY_MNG_STAFF_FK.ToString()));
                }
                objVal = oRes.INTRO_C3K12_LOCAL_LAW_ENFORCEMENT_FK;
                if (objVal != null)
                {
                    rblLawEnforcement.SelectedIndex = rblLawEnforcement.Items.IndexOf(rblLawEnforcement.Items.FindByValue(oRes.INTRO_C3K12_LOCAL_LAW_ENFORCEMENT_FK.ToString()));

                }
                objVal = oRes.INTRO_C4K12_EMS_PERSONNEL_FK;
                if (objVal != null)
                {
                    rbtEMSpersonnel.SelectedIndex = rbtEMSpersonnel.Items.IndexOf(rbtEMSpersonnel.Items.FindByValue(oRes.INTRO_C4K12_EMS_PERSONNEL_FK.ToString()));
                }

                objVal = oRes.INTRO_C5K12_SCHOOL_RESOURCE_OFF_FK;
                if (objVal != null)
                {
                    rbtSchoolResource.SelectedIndex = rbtSchoolResource.Items.IndexOf(rbtSchoolResource.Items.FindByValue(oRes.INTRO_C5K12_SCHOOL_RESOURCE_OFF_FK.ToString()));
                }


                objVal = oRes.INTRO_C6K12_FIRE_OFFICALS_FK;
                if (objVal != null)
                {
                    rbtFireOfficials.SelectedIndex = rbtFireOfficials.Items.IndexOf(rbtFireOfficials.Items.FindByValue(oRes.INTRO_C6K12_FIRE_OFFICALS_FK.ToString()));
                }
                objVal = oRes.INTRO_C7K12_PUBLIC_MENTAL_PRACTITIONERS_FK;
                if (objVal != null)
                {
                    rbtHealthPractitioners.SelectedIndex = rbtHealthPractitioners.Items.IndexOf(rbtHealthPractitioners.Items.FindByValue(oRes.INTRO_C7K12_PUBLIC_MENTAL_PRACTITIONERS_FK.ToString()));
                }


                objVal = oRes.INTRO_C8K12_LOCAL_EMERGENCY_MNG_FK;
                if (objVal != null)
                {
                    rbtLocalEmergency.SelectedIndex = rbtLocalEmergency.Items.IndexOf(rbtLocalEmergency.Items.FindByValue(oRes.INTRO_C8K12_LOCAL_EMERGENCY_MNG_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            K12_ASSESSMENT_QUESTION oRes = (from c in db.K12_ASSESSMENT_QUESTIONs
                                            where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new K12_ASSESSMENT_QUESTION();
                blNew = true;

            }

            if (!(rbtEmergencyPlan.SelectedItem == null))
            {
                if (!(rbtEmergencyPlan.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_AK12_EMERGENCY_PLAN_FK = Convert.ToInt32(rbtEmergencyPlan.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_AK12_EMERGENCY_PLAN_FK = null;
                }
            }
            if (!(rbtIndividualTeamSafety.SelectedItem == null))
            {
                if (!(rbtIndividualTeamSafety.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_BK12_INDIVIDUAL_OR_TEAM_SAFETY_FK = Convert.ToInt32(rbtIndividualTeamSafety.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_BK12_INDIVIDUAL_OR_TEAM_SAFETY_FK = null;
                }
            }
            if (!(rbtFirstResponders.SelectedItem == null))
            {
                if (!(rbtFirstResponders.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C1K12_FIRST_RESPONDERS_FK = Convert.ToInt32(rbtFirstResponders.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C1K12_FIRST_RESPONDERS_FK = null;
                }
            }
            if (!(rbtLocalEmergencyStaff.SelectedItem == null))
            {
                if (!(rbtLocalEmergencyStaff.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C2K12_LOCAL_EMERGENCY_MNG_STAFF_FK = Convert.ToInt32(rbtLocalEmergencyStaff.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C2K12_LOCAL_EMERGENCY_MNG_STAFF_FK = null;
                }
            }
            if (!(rblLawEnforcement.SelectedItem == null))
            {
                if (!(rblLawEnforcement.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C3K12_LOCAL_LAW_ENFORCEMENT_FK = Convert.ToInt32(rblLawEnforcement.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C3K12_LOCAL_LAW_ENFORCEMENT_FK = null;
                }
            }
            if (!(rbtEMSpersonnel.SelectedItem == null))
            {
                if (!(rbtEMSpersonnel.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C4K12_EMS_PERSONNEL_FK = Convert.ToInt32(rbtEMSpersonnel.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C4K12_EMS_PERSONNEL_FK = null;
                }
            }


            if (!(rbtSchoolResource.SelectedItem == null))
            {
                if (!(rbtSchoolResource.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C5K12_SCHOOL_RESOURCE_OFF_FK = Convert.ToInt32(rbtSchoolResource.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C5K12_SCHOOL_RESOURCE_OFF_FK = null;
                }
            }



            if (!(rbtFireOfficials.SelectedItem == null))
            {
                if (!(rbtFireOfficials.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C6K12_FIRE_OFFICALS_FK = Convert.ToInt32(rbtFireOfficials.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C6K12_FIRE_OFFICALS_FK = null;
                }
            }
            if (!(rbtHealthPractitioners.SelectedItem == null))
            {
                if (!(rbtHealthPractitioners.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C7K12_PUBLIC_MENTAL_PRACTITIONERS_FK = Convert.ToInt32(rbtHealthPractitioners.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C7K12_PUBLIC_MENTAL_PRACTITIONERS_FK = null;
                }
            }
            if (!(rbtLocalEmergency.SelectedItem == null))
            {
                if (!(rbtLocalEmergency.SelectedItem.Value.ToString() == ""))
                {
                    oRes.INTRO_C8K12_LOCAL_EMERGENCY_MNG_FK = Convert.ToInt32(rbtLocalEmergency.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.INTRO_C8K12_LOCAL_EMERGENCY_MNG_FK = null;
                }
            }


            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.K12_ASSESSMENT_QUESTIONs.InsertOnSubmit(oRes);
            }
            db.SubmitChanges();
          //  lngPkID = oRes.K12_ASSESSMENT_QUESID;
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.K12_YESNO_NOTs
                      select e;


            rbtFirstResponders.DataSource = qry;
            rbtFirstResponders.DataTextField = "Yes_No_Not";
            rbtFirstResponders.DataValueField = "NotID";
            rbtFirstResponders.DataBind();

            rbtEmergencyPlan.DataSource = qry;
            rbtEmergencyPlan.DataTextField = "Yes_No_Not";
            rbtEmergencyPlan.DataValueField = "NotID";
            rbtEmergencyPlan.DataBind();

            rbtIndividualTeamSafety.DataSource = qry;
            rbtIndividualTeamSafety.DataTextField = "Yes_No_Not";
            rbtIndividualTeamSafety.DataValueField = "NotID";
            rbtIndividualTeamSafety.DataBind();

            rbtFirstResponders.DataSource = qry;
            rbtIndividualTeamSafety.DataTextField = "Yes_No_Not";
            rbtIndividualTeamSafety.DataValueField = "NotID";
            rbtIndividualTeamSafety.DataBind();

            rbtLocalEmergencyStaff.DataSource = qry;
            rbtLocalEmergencyStaff.DataTextField = "Yes_No_Not";
            rbtLocalEmergencyStaff.DataValueField = "NotID";
            rbtLocalEmergencyStaff.DataBind();


            rblLawEnforcement.DataSource = qry;
            rblLawEnforcement.DataTextField = "Yes_No_Not";
            rblLawEnforcement.DataValueField = "NotID";
            rblLawEnforcement.DataBind();

            rbtEMSpersonnel.DataSource = qry;
            rbtEMSpersonnel.DataTextField = "Yes_No_Not";
            rbtEMSpersonnel.DataValueField = "NotID";
            rbtEMSpersonnel.DataBind();


            rbtSchoolResource.DataSource = qry;
            rbtSchoolResource.DataTextField = "Yes_No_Not";
            rbtSchoolResource.DataValueField = "NotID";
            rbtSchoolResource.DataBind();


            rbtFireOfficials.DataSource = qry;
            rbtFireOfficials.DataTextField = "Yes_No_Not";
            rbtFireOfficials.DataValueField = "NotID";
            rbtFireOfficials.DataBind();

            rbtHealthPractitioners.DataSource = qry;
            rbtHealthPractitioners.DataTextField = "Yes_No_Not";
            rbtHealthPractitioners.DataValueField = "NotID";
            rbtHealthPractitioners.DataBind();

            rbtLocalEmergency.DataSource = qry;
            rbtLocalEmergency.DataTextField = "Yes_No_Not";
            rbtLocalEmergency.DataValueField = "NotID";
            rbtLocalEmergency.DataBind();


        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("C_ThreatHazards.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("A_Demographic.aspx?lngPkID=" + lngPkID);
    }
}