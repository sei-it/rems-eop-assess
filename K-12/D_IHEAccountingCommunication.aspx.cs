﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class D_IHEAccountingCommunication : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "4/10";
    }


    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {

            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {

                objVal = oRes.ACCOUNT_COMM_A1_BEFORE_EMG_EVENT_FK;
                if (objVal != null)
                {
                    rbtBeforeEmergency.SelectedIndex = rbtBeforeEmergency.Items.IndexOf(rbtBeforeEmergency.Items.FindByValue(oRes.ACCOUNT_COMM_A1_BEFORE_EMG_EVENT_FK.ToString()));
                }
                objVal = oRes.ACCOUNT_COMM_A2_DURING_EMG_EVENT_FK;
                if (objVal != null)
                {
                    rbtDuringEmergency.SelectedIndex = rbtDuringEmergency.Items.IndexOf(rbtDuringEmergency.Items.FindByValue(oRes.ACCOUNT_COMM_A2_DURING_EMG_EVENT_FK.ToString()));
                }
                objVal = oRes.ACCOUNT_COMM_A3_AFTER_EMG_EVENT_FK;
                if (objVal != null)
                {
                    rbtAfterEmergency.SelectedIndex = rbtAfterEmergency.Items.IndexOf(rbtAfterEmergency.Items.FindByValue(oRes.ACCOUNT_COMM_A3_AFTER_EMG_EVENT_FK.ToString()));
                }
                objVal = oRes.ACCOUNT_COMM_B1_ANNOUNCING_PRO_TOFOLLOW_FK;
                if (objVal != null)
                {
                    rbtAnnouncing.SelectedIndex = rbtAnnouncing.Items.IndexOf(rbtAnnouncing.Items.FindByValue(oRes.ACCOUNT_COMM_B1_ANNOUNCING_PRO_TOFOLLOW_FK.ToString()));
                }
                objVal = oRes.ACCOUNT_COMM_B2_COMMUNICATING_PRESS_MEDIA_FK;
                if (objVal != null)
                {
                    rbtCommunicating.SelectedIndex = rbtCommunicating.Items.IndexOf(rbtCommunicating.Items.FindByValue(oRes.ACCOUNT_COMM_B2_COMMUNICATING_PRESS_MEDIA_FK.ToString()));
                }


            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.IHE_K12_FK == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;

            }

            if (!(rbtBeforeEmergency.SelectedItem == null))
            {
                if (!(rbtBeforeEmergency.SelectedItem.Value.ToString() == ""))
                {
                    oRes.ACCOUNT_COMM_A1_BEFORE_EMG_EVENT_FK = Convert.ToInt32(rbtBeforeEmergency.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.ACCOUNT_COMM_A1_BEFORE_EMG_EVENT_FK = null;
                }
            }
            if (!(rbtDuringEmergency.SelectedItem == null))
            {
                if (!(rbtDuringEmergency.SelectedItem.Value.ToString() == ""))
                {
                    oRes.ACCOUNT_COMM_A2_DURING_EMG_EVENT_FK = Convert.ToInt32(rbtDuringEmergency.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.ACCOUNT_COMM_A2_DURING_EMG_EVENT_FK = null;
                }
            }
            if (!(rbtAfterEmergency.SelectedItem == null))
            {
                if (!(rbtAfterEmergency.SelectedItem.Value.ToString() == ""))
                {
                    oRes.ACCOUNT_COMM_A3_AFTER_EMG_EVENT_FK = Convert.ToInt32(rbtAfterEmergency.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_B2_PROTECT_FK = null;
                }
            }
            if (!(rbtAnnouncing.SelectedItem == null))
            {
                if (!(rbtAnnouncing.SelectedItem.Value.ToString() == ""))
                {
                    oRes.ACCOUNT_COMM_B1_ANNOUNCING_PRO_TOFOLLOW_FK = Convert.ToInt32(rbtAnnouncing.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.ACCOUNT_COMM_B1_ANNOUNCING_PRO_TOFOLLOW_FK = null;
                }
            }
            if (!(rbtCommunicating.SelectedItem == null))
            {
                if (!(rbtCommunicating.SelectedItem.Value.ToString() == ""))
                {
                    oRes.ACCOUNT_COMM_B2_COMMUNICATING_PRESS_MEDIA_FK = Convert.ToInt32(rbtCommunicating.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.ACCOUNT_COMM_B2_COMMUNICATING_PRESS_MEDIA_FK = null;
                }
            }


            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.YesNo_Nots
                      select e;

            rbtBeforeEmergency.DataSource = qry;
            rbtBeforeEmergency.DataTextField = "Yes_No_Not";
            rbtBeforeEmergency.DataValueField = "NotID";
            rbtBeforeEmergency.DataBind();

            rbtDuringEmergency.DataSource = qry;
            rbtDuringEmergency.DataTextField = "Yes_No_Not";
            rbtDuringEmergency.DataValueField = "NotID";
            rbtDuringEmergency.DataBind();

            rbtAfterEmergency.DataSource = qry;
            rbtAfterEmergency.DataTextField = "Yes_No_Not";
            rbtAfterEmergency.DataValueField = "NotID";
            rbtAfterEmergency.DataBind();

            rbtAnnouncing.DataSource = qry;
            rbtAnnouncing.DataTextField = "Yes_No_Not";
            rbtAnnouncing.DataValueField = "NotID";
            rbtAnnouncing.DataBind();

            rbtCommunicating.DataSource = qry;
            rbtCommunicating.DataTextField = "Yes_No_Not";
            rbtCommunicating.DataValueField = "NotID";
            rbtCommunicating.DataBind();

        }
    }


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnBackIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("C_IHEThreatHazards.aspx?lngPkID=" + lngPkID);
    }
    protected void btnNextIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("E_IHEPlanningEduCommunity.aspx?lngPkID=" + lngPkID);
    }
}