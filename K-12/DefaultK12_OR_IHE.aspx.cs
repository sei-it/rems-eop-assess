﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DefaultK12_OR_IHE : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }


        }

        if (ViewState["IsLoaded1"] == null)
        {
            //displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void rbtk12OrIHE_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtk12OrIHE.SelectedValue == "1")
        {
            Response.Redirect("A_Demographic.aspx?lngPkID=" + lngPkID);
        }
        else
        {

            Response.Redirect("http://devremsr2.seiservices.com/assessment/IHE/A_Demographic.aspx");        
        }
    }
  
}