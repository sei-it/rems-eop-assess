﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class F_IHELawsRegulations : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "6/10";
    }




    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oRess = from css in db.IHE_AssessmentQuestions
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.LAW_REGULATIONS_A_AWARE_LOCAL_STATE_FEDERAL_LAW_FK;
                if (objVal != null)
                {
                    rbtLocalStateFederalLaw.SelectedIndex = rbtLocalStateFederalLaw.Items.IndexOf(rbtLocalStateFederalLaw.Items.FindByValue(oRes.LAW_REGULATIONS_A_AWARE_LOCAL_STATE_FEDERAL_LAW_FK.ToString()));
                }
                objVal = oRes.LAW_REGULATIONS_B_HAVE_EMERGENCY_PLAN_FK;
                if (objVal != null)
                {
                    rbtAlreadyHaveEmgPlan.SelectedIndex = rbtAlreadyHaveEmgPlan.Items.IndexOf(rbtAlreadyHaveEmgPlan.Items.FindByValue(oRes.LAW_REGULATIONS_B_HAVE_EMERGENCY_PLAN_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.IHE_K12_FK == lngPkID
                                           select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;
            }

            if (!(rbtLocalStateFederalLaw.SelectedItem == null))
            {
                if (!(rbtLocalStateFederalLaw.SelectedItem.Value.ToString() == ""))
                {
                    oRes.LAW_REGULATIONS_A_AWARE_LOCAL_STATE_FEDERAL_LAW_FK = Convert.ToInt32(rbtLocalStateFederalLaw.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.LAW_REGULATIONS_A_AWARE_LOCAL_STATE_FEDERAL_LAW_FK = null;
                }
            }
            if (!(rbtAlreadyHaveEmgPlan.SelectedItem == null))
            {
                if (!(rbtAlreadyHaveEmgPlan.SelectedItem.Value.ToString() == ""))
                {
                    oRes.LAW_REGULATIONS_B_HAVE_EMERGENCY_PLAN_FK = Convert.ToInt32(rbtAlreadyHaveEmgPlan.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.LAW_REGULATIONS_B_HAVE_EMERGENCY_PLAN_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.YesNos
                      select e;

            rbtLocalStateFederalLaw.DataSource = qry;
            rbtLocalStateFederalLaw.DataTextField = "YES_NO";
            rbtLocalStateFederalLaw.DataValueField = "YESNOId";
            rbtLocalStateFederalLaw.DataBind();

            var qry2 = from e in db.YesNo_Nots
                       select e;

            rbtAlreadyHaveEmgPlan.DataSource = qry2;
            rbtAlreadyHaveEmgPlan.DataTextField = "Yes_No_Not";
            rbtAlreadyHaveEmgPlan.DataValueField = "NotID";
            rbtAlreadyHaveEmgPlan.DataBind();


        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNextIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("G_IHETrainingDocumentation.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBackIHE_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("E_IHEPlanningEduCommunity.aspx?lngPkID=" + lngPkID);
    }
}