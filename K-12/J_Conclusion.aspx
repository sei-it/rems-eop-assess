﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="J_Conclusion.aspx.cs" Inherits="J_Conclusion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div id="active10"></div>
<h1>Conclusion</h1>


<fieldset class="assessFormIntro">
  <p >
      Thank you for taking the time to complete EOP ASSESS, designed to help you gauge your capacity to create and maintain a 
      high-quality school emergency operations plan (EOP) that considers a range of threats and hazards. 
      You are now familiar with the key considerations involved and important questions to consider as 
      you engage in the process of developing a high-quality plan.</p>
<p>
Upon selecting Download, you will be presented with a customized, downloadable report containing a variety of 
      additional resources and information, based upon your responses to the self-assessment questions. 
      For additional information on the planning process recommended by the U.S. Departments of Education, 
      Homeland Security, Justice, and Health and Human Services, as well as on questions and concerns 
      regarding your report, please contact the Readiness and Emergency Management for Schools (REMS) 
      Technical Assistance (TA) Center Help Desk by email <a href="mailTo:info@remstacenter.org">(info@remstacenter.org)</a> or by telephone (855-781-7367 [REMS]), 
      toll-free Monday through Friday, between the hours of 9:00 a.m. and 5:00 p.m. Eastern Time.

     <%-- Thank you for taking time to complete this self-assessment, designed to help you gauge your 
      capacity to create and maintain a high-quality school emergency operations plan that considers 
      a range of threats and hazards. You are now familiar with the key considerations involved and 
      important questions to consider as you engage in the process of developing a high-quality plan. 
      Upon clicking the “next” button, you will be presented with a customized downloadable report 
      containing a variety of additional resources and information, based upon your responses to the 
      assessment questions. For additional information on the planning process as recommended by the 
      U.S. Departments of Education, Homeland Security, Justice, and Health and Human Services, as 
      well as on questions and concerns regarding your report, please contact the Readiness and Emergency 
      Management for Schools (REMS) Technical Assistance (TA) Center Help Desk by email <a href="mailTo:info@remstacenter.org"> (info@remstacenter.org)</a>
      or by telephone (855-781-7367 [REMS]), toll free and between the hours of 9:00 a.m. and 5:00 p.m., Eastern 
      Standard Time. This concludes the self-assessment. Click DOWNLOAD to generate your report, which you may 
      then save to your desktop in PDF format. Thank you.--%>
      
  </p>
   
       
</fieldset>
     <div class="prevNext">       
       <asp:Button ID="btnBack" runat="server" Visible="false" Text="Go Back" OnClick="btnBack_Click" />
       <asp:Button ID="btnNext" runat="server" Visible="true" Text="DOWNLOAD" OnClick="btnNext_Click" />
        
    </div>
   <%--  <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">11/11</p></div>--%>
</asp:Content>


