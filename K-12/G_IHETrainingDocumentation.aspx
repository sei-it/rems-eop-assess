﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="G_IHETrainingDocumentation.aspx.cs" Inherits="G_IHETrainingDocumentation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <h1>Training and Documentation</h1>
  
<fieldset class="assessForm">
            <legend for="rbtAccessibleToolsHandy">14. Does your school provide tools <a class="MouesoverPopup" >and documents</a> handy that contain instructions
            for students, faculty, staff, and visitors for use during an emergency?

            <asp:RequiredFieldValidator ID="reqAccessibleToolsHandy" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAccessibleToolsHandy"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </legend>
             <asp:RadioButtonList ID="rbtAccessibleToolsHandy" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
      
   </fieldset>

<fieldset class="assessForm">
            <legend for="rbtIHEpostImp">15. Does your IHE post important information such as exit locations and 
             evacuation routes throughout the building and in plain view for use during an emergency event?
             <asp:RequiredFieldValidator ID="reqIHEpostImp" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtIHEpostImp"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </legend>
            <asp:RadioButtonList ID="rbtIHEpostImp" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
    </fieldset>

<fieldset class="assessForm">
            <legend for="rbtAccessibleToolComply">16. Do the accessible tools, documents, and posted information mentioned above comply
            with the requirements put forth in the <a class="MouesoverPopup" ><i>Americans with Disabilities Act (ADA)</i> guidance</a>?

            <asp:RequiredFieldValidator ID="reqAccessibleToolComply" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtAccessibleToolComply"
                  ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </legend>
            <asp:RadioButtonList ID="rbtAccessibleToolComply" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>
       
  </fieldset>

<div class="prevNext">       
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNextIHE_Click" />
        
</div>
   

     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red"  CssClass="ValidationSummary"
       HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

</asp:Content>


