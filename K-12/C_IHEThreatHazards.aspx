﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="C_IHEThreatHazards.aspx.cs" Inherits="C_IHEThreatHazards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
     
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
     <h1>Threats and Hazards</h1>
    <p> In addition to thinking about who should be on your planning team, let’s think about what types of emergency events you and your team will have to plan for. These are generally called "threats" and "hazards."
 </p>
<fieldset class="assessForm multiPart">  
        
       <label for="rbtListOfRiskThreats"><legend>4. Have you or your IHE’s planning team thought about or made a list 
                     of the threats and hazards most likely to impact your campus or surrounding community? </legend>
             <asp:RequiredFieldValidator ID="reqListOfRiskThreats" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtListOfRiskThreats"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
            <asp:RadioButtonList ID="rbtListOfRiskThreats" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
            </asp:RadioButtonList>

       <label  class="noBorder1" for="rbtEmergencyPlan" >Remember, threats are human-caused emergencies such as fire, gang violence, cyber-attacks, and active shooters, while hazards may be natural disasters, such as earthquakes and floods, and technological, such as hazardous materials releases and gas leaks or laboratory spills.
    </label>

     </fieldset>

<fieldset class="assessForm multiPart">   
    <legend>5. Whether or not your IHE has thought about or made a list of the risks or threats and hazards most likely 
        to impact your campus or surrounding community, have you considered strategies to:</legend>
    <div class="questionGroup">
                <label for="rbtPrevent"><a class="MouesoverPopup" >Prevent</a> an emergency event from impacting your campus/IHE?
                  <asp:RequiredFieldValidator ID="reqPrevent" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtPrevent"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                 </label>
                <asp:RadioButtonList ID="rbtPrevent" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
        
    <label class="defined" for="rbtPrevent"><i>Prevention is the action IHEs take to prevent a threatened or actual incident from occurring. For example, reviewing recent community and campus-specific crime data.</i></label>
    </div><!-- /questionGroup -->
              <div class="questionGroup">
               <label for="rbtProtect"><a class="MouesoverPopup" ">Protect</a>  against the adverse effects of an emergency event? 
                <asp:RequiredFieldValidator ID="reqProtect" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtProtect"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                 </label>
                <asp:RadioButtonList ID="rbtProtect" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>   

    <label class="defined" for="rbtProtect"><i>Protection focuses on ongoing actions that protect students, faculty, staff, visitors, networks, and property from a threat or hazard. For example, conducting hurricane drills can reduce injury to students, faculty, and staff because they will know what to do to avoid harm.</i></label>
     </div><!-- /questionGroup -->
            
            <div class="questionGroup">
             <label for="rbtMitigate"><a class="MouesoverPopup" >Mitigate</a> the effects of an emergency event?
             <asp:RequiredFieldValidator ID="reqMitigate" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtMitigate"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
              </label>
             <asp:RadioButtonList ID="rbtMitigate" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
             </asp:RadioButtonList>
   
   <label class="defined" for="rbtMitigate"><i>Mitigation is the capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the impact of an event or emergency. For example, improving surveillance capabilities and access controls may mitigate some emergencies.</i></label>
     </div><!-- /questionGroup -->
   <div class="questionGroup">       
     <label for="rbtRespond"><a class="MouesoverPopup" >Respond</a> to an emergency event? 
             <asp:RequiredFieldValidator ID="reqRespond" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtRespond"
                   ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
             </label>
             <asp:RadioButtonList ID="rbtRespond" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
             </asp:RadioButtonList>              
    <label class="defined" for="rbtRespond" ><i>Response is the capabilities necessary to stabilize an emergency once it has already happened or is certain to happen in an unpreventable way. For example, evacuating or sheltering-in-place, as appropriate.</i></label>
   
       </div><!-- /questionGroup --> 
        <div class="questionGroup">               
     <label for="rbtRecover"><a class="MouesoverPopup" >Recover</a> from an emergency event? 
               <asp:RequiredFieldValidator ID="reqRecover" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtRecover"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
                </label>
                <asp:RadioButtonList ID="rbtRecover" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
         
    <label class="defined" for="rbtRecover"><i>Recovery is the capabilities necessary to assist IHEs affected by an event or emergency in restoring the learning environment. For example, providing stress management and mental health resources to students, faculty, and staff following an emergency event.</i></label>
   </div><!-- /questionGroup -->
            
              </fieldset>

<fieldset class="assessForm multiPart">  
  <legend>6. Your IHE may already employ strategies for responding to a variety of threats or hazards. For example, when you practice regular drills, such as fire drills, you are practicing 
      ways to respond to specific events. Does your IHE currently practice any of the following drills?</legend>
     <div class="questionGroup">  
            <label for="rbtDenyEntry"><a class="MouesoverPopup" >Deny Entry or Closing (Lockdown):</a> <i>Secure IHE buildings, facilities, and grounds during incidents that pose an immediate threat or violence in or around the IHE.</i>
            <asp:RequiredFieldValidator ID="reqDenyEntry" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtDenyEntry"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtDenyEntry" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow" >
            </asp:RadioButtonList>
              </div><!-- /questionGroup -->

         <div class="questionGroup">  
             <label for="rbtEvacuation"><a class="MouesoverPopup" >Evacuation:</a><i> Evacuate IHE buildings, facilities, and grounds.</i>   
              <asp:RequiredFieldValidator ID="reqEvacuation" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtEvacuation"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
              </label>
              <asp:RadioButtonList ID="rbtEvacuation" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
              </asp:RadioButtonList>
       </div><!-- /questionGroup -->

        <div class="questionGroup">  
              <label for="rbtShelterInPlace"><a class="MouesoverPopup" >Shelter-in-Place/Secure-in-Place:</a> <i>Students, faculty, and staff remain indoors, potentially for an extended period of time, because it is safer inside the building or a room than outside.</i>
               <asp:RequiredFieldValidator ID="reqShelterInPlace" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtShelterInPlace"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
               <asp:RadioButtonList ID="rbtShelterInPlace" CssClass="leftAlign" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
               </asp:RadioButtonList>     
        </div><!-- /questionGroup -->
      
     </fieldset>
  

    <div class="prevNext">
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE" runat="server" ValidationGroup="Save" Text="Next" OnClick="btnNextIHE_Click" />
        
    </div>
      
     <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" CssClass="ValidationSummary"
         HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>

     <div id='PopUp3' class="PopUp" ><span class="b-close"></span>
              <p><span>Threats</span> are human-caused emergencies such as fire, gang violence, cyber-attacks, and <em>active shooters</em>.</p>
              
    </div>
    <div id='PopUp4' class="PopUp" ><span class="b-close"></span>
              <p><span>Hazards</span> may be natural disasters, such as earthquakes, wildfires, and floods, technological, such as hazardous materials releases, and biological, such as infectious disease outbreaks.</p>
                
              
    </div>
     <div id='PopUp5' class="PopUp" ><span class="b-close"></span>
              <p><span>Prevention:</span> The action IHEs take to prevent a threatened 
                  or actual incident from occurring. For example, reviewing recent community and campus-specific crime data.</p>
              
    </div>
     <div id='PopUp6' class="PopUp" ><span class="b-close"></span>
              <p><span>Protection:</span> Protection focuses on ongoing actions that protect students, faculty, staff, visitors,
                   networks, and property from a threat or hazard. For example, conducting hurricane drills can reduce 
                  injury to students, faculty, and staff because they will know what to do to avoid harm. </p>
    </div>
     <div id='PopUp7' class="PopUp" ><span class="b-close"></span>
              <p><span>Mitigation:</span> The capabilities necessary to eliminate or reduce the loss of life and property damage by lessening the impact of an 
                  event or emergency. For example, improving surveillance capabilities and access controls may mitigate some emergencies.</p>
              
    </div>
     <div id='PopUp8' class="PopUp" ><span class="b-close"></span>
              <p><span>Response:</span> The capabilities necessary to stabilize an emergency once it has already happened or is certain to happen 
                  in an unpreventable way. For example, evacuating or sheltering-in-place, as appropriate.</p>
    </div>
     <div id='PopUp9' class="PopUp" ><span class="b-close"></span>
              <p><span>Recovery:</span> The capabilities necessary to assist IHEs affected by an event or emergency in restoring the learning environment. For example, 
                  providing stress management and mental health resources to students, faculty, and staff following an emergency event.</p>
              
    </div>
     <div id='PopUp10' class="PopUp" ><span class="b-close"></span>
              <p><span>Deny Entry or Closing:</span> Secure IHE buildings, facilities, and
                   grounds during incidents that pose an immediate threat or violence in or around the IHE.</p>
              
    </div>
     <div id='PopUp11' class="PopUp" ><span class="b-close"></span>
              <p><span>Evacuation:</span> Evacuate IHE buildings and grounds.</p>
                
              
    </div>
     <div id='PopUp12' class="PopUp" ><span class="b-close"></span>
              <p><span>Shelter-in-place:</span> Students and staff remain indoors, perhaps for an extended period of time, because it is safer 
                  inside the building or a room than outside.</p>
                    
              
    </div>
</asp:Content>


