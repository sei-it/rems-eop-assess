﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="D_AccountingCommunication.aspx.cs" Inherits="D_AccountingCommunication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div id="active4"></div>
    <h1>Accounting for All Persons and Communications and Warning</h1>

<fieldset class="assessForm multiPart">  
  <legend>7. Do you have a designated person(s) for ensuring  that all students, staff, 
      and visitors are accounted for: </legend>

        <div class="questionGroup">
               <label for="rbtBeforeAnEmergency"><a class="MouesoverPopup" >Before </a> an emergency event?
        
                    <asp:RequiredFieldValidator ID="reqBeforeEmergency" runat="server" 
                             ValidationGroup="Save" ControlToValidate="rbtBeforeEmergency"
                             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
                <asp:RadioButtonList ID="rbtBeforeEmergency" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        
                </asp:RadioButtonList>
        </div><!-- /questionGroup -->

        <div class="questionGroup">
               <label for="rbtDuringEmergency"><a class="MouesoverPopup" >During</a> an emergency event?
                    <asp:RequiredFieldValidator ID="reqDuringEmergency" runat="server" 
                             ValidationGroup="Save" ControlToValidate="rbtDuringEmergency"
                             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
                <asp:RadioButtonList ID="rbtDuringEmergency" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        
                </asp:RadioButtonList>
        </div><!-- /questionGroup -->

        <div class="questionGroup">
               <label for="rbtAfterEmergency"><a class="MouesoverPopup" >After</a> an emergency event? 
                    <asp:RequiredFieldValidator ID="reqAfterEmergency" runat="server" 
                             ValidationGroup="Save" ControlToValidate="rbtAfterEmergency"
                             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
                <asp:RadioButtonList ID="rbtAfterEmergency" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
                </asp:RadioButtonList>
        </div><!-- /questionGroup -->

 </fieldset>

 <fieldset class="assessForm multiPart">
  <legend>8. Before, during, and after an emergency, it is crucial to 
      be able to communicate with others. Do you have a designated person(s) for any of the following?</legend>

        <div class="questionGroup">
               <label for="rbtAnnouncing">Announcing what procedures to follow during an emergency event?
                     <asp:RequiredFieldValidator ID="reqAnnouncing" runat="server" 
                             ValidationGroup="Save" ControlToValidate="rbtAnnouncing"
                            ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
               </label>
                <asp:RadioButtonList ID="rbtAnnouncing" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        
                </asp:RadioButtonList>
        </div><!-- /questionGroup -->

        <div class="questionGroup">
               <label for="rbtCommunicating">Communicating with the press, media, and families following an emergency event?
                    <asp:RequiredFieldValidator ID="reqCommunicating" runat="server" 
                             ValidationGroup="Save" ControlToValidate="rbtCommunicating"
                             ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
        
               </label>
                <asp:RadioButtonList ID="rbtCommunicating" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        
                </asp:RadioButtonList>
        </div><!-- /questionGroup -->
  </fieldset>

    <div class="prevNext">       
        <asp:Button ID="btnBack" runat="server" Text="Go Back" OnClick="btnBack_Click" />
        <asp:Button ID="btnNext" runat="server"  ValidationGroup="Save" Text="Next" OnClick="btnNext_Click" />
         
   <%-- </div>
     <div  style="padding-top:50px;font:bolder;font-size:larger">
    <p align="center">5/11</p></div>--%>
  
        <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" ValidationGroup="Save" runat="server" ForeColor="Red" 
             HeaderText="All required information was not provided. Please answer the marked question(s)." />
     <asp:Literal ID="litMessage" runat="server"></asp:Literal>
</asp:Content>


