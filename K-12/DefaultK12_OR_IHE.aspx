﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsMasterPage.master" AutoEventWireup="true" CodeFile="DefaultK12_OR_IHE.aspx.cs" Inherits="DefaultK12_OR_IHE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <h1>Select K-12 or IHE</h1>
    <div>

     <fieldset class="assessForm">
             <legend for="rbtk12OrIHE">Are you affiliated with a K-12 School/District or with an Institute of Higher Education?                             
                  
                 <asp:RequiredFieldValidator ID="reqIHEK12" runat="server" 
                  ValidationGroup="Save" ControlToValidate="rbtk12OrIHE"
                  ErrorMessage="Is required field" ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
          
            </legend>
            <asp:RadioButtonList ID="rbtk12OrIHE" runat="server" RepeatDirection="Vertical" AutoPostBack="true"  OnSelectedIndexChanged="rbtk12OrIHE_SelectedIndexChanged" RepeatLayout="Flow">
            <asp:ListItem  Text="K-12" Value="1"></asp:ListItem>
            <asp:ListItem Text="IHE" Value="2"></asp:ListItem>
            </asp:RadioButtonList> 

     </fieldset>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>

