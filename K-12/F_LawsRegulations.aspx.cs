﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class F_LawsRegulations : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "6/10";
    }




    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.LAW_REG_AK12_AWARE_LOCAL_STATE_FEDERAL_LAW_FK;
                if (objVal != null)
                {
                    rbtLocalStateFederalLaw.SelectedIndex = rbtLocalStateFederalLaw.Items.IndexOf(rbtLocalStateFederalLaw.Items.FindByValue(oRes.LAW_REG_AK12_AWARE_LOCAL_STATE_FEDERAL_LAW_FK.ToString()));
                }
                objVal = oRes.LAW_REG_BK12_HAVE_EMERGENCY_PLAN_FK;
                if (objVal != null)
                {
                    rbtAlreadyHaveEmgPlan.SelectedIndex = rbtAlreadyHaveEmgPlan.Items.IndexOf(rbtAlreadyHaveEmgPlan.Items.FindByValue(oRes.LAW_REG_BK12_HAVE_EMERGENCY_PLAN_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            K12_ASSESSMENT_QUESTION oRes = (from c in db.K12_ASSESSMENT_QUESTIONs
                                            where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new K12_ASSESSMENT_QUESTION();
                blNew = true;

            }

            if (!(rbtLocalStateFederalLaw.SelectedItem == null))
            {
                if (!(rbtLocalStateFederalLaw.SelectedItem.Value.ToString() == ""))
                {
                    oRes.LAW_REG_AK12_AWARE_LOCAL_STATE_FEDERAL_LAW_FK = Convert.ToInt32(rbtLocalStateFederalLaw.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.LAW_REG_AK12_AWARE_LOCAL_STATE_FEDERAL_LAW_FK = null;
                }
            }
            if (!(rbtAlreadyHaveEmgPlan.SelectedItem == null))
            {
                if (!(rbtAlreadyHaveEmgPlan.SelectedItem.Value.ToString() == ""))
                {
                    oRes.LAW_REG_BK12_HAVE_EMERGENCY_PLAN_FK = Convert.ToInt32(rbtAlreadyHaveEmgPlan.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.LAW_REG_BK12_HAVE_EMERGENCY_PLAN_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.K12_ASSESSMENT_QUESTIONs.InsertOnSubmit(oRes);
            }
            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.k12_YESNOs
                      select e;

            rbtLocalStateFederalLaw.DataSource = qry;
            rbtLocalStateFederalLaw.DataTextField = "YES_NO";
            rbtLocalStateFederalLaw.DataValueField = "YESNOId";
            rbtLocalStateFederalLaw.DataBind();

            var qry2 = from e in db.K12_YESNO_NOTs
                       select e;

            rbtAlreadyHaveEmgPlan.DataSource = qry2;
            rbtAlreadyHaveEmgPlan.DataTextField = "Yes_No_Not";
            rbtAlreadyHaveEmgPlan.DataValueField = "NotID";
            rbtAlreadyHaveEmgPlan.DataBind();


        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("G_TrainingDocumentation.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("E_PlanningEduCommunity.aspx?lngPkID=" + lngPkID);
    }
}