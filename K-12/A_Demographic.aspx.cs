﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class A_Demographic : System.Web.UI.Page
{
    int lngPkID;
    int intUserId;
    string Pagetext;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }

        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Page.MaintainScrollPositionOnPostBack = true;
        Session["Pagetext"] = "1/10";
    }

    protected void displayRecords()
    {
        object objVal = null;
        object objVal1 = null;
        using ( DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oIhek = from k in db.IHE_K12s where k.K12_IHE_Id == lngPkID select k;

            foreach (var ocase in oIhek)
            {
               objVal1 = ocase.K12_IHE_Selected;
               if (objVal1 != null)
               {
                   rbtk12OrIHE.SelectedIndex = rbtk12OrIHE.Items.IndexOf(rbtk12OrIHE.Items.FindByValue(ocase.K12_IHE_Selected.ToString()));
               }
            }


            if (rbtk12OrIHE.SelectedIndex==0)
            {
                loadK12Dropdown();
                var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                            where css.IHE_K12_FK == lngPkID
                            select css;
                foreach (var oRes in oRess)
                {

                    objVal = oRes.DEMO_AK12_ORGANIZATION_FROM_FK;
                    if (objVal != null)
                    {
                        ddlOrganizationFrom.SelectedIndex = ddlOrganizationFrom.Items.IndexOf(ddlOrganizationFrom.Items.FindByValue(oRes.DEMO_AK12_ORGANIZATION_FROM_FK.ToString()));
                    }
                    objVal = oRes.DEMO_BK12_YOUR_ROLE_FK;
                    if (objVal != null)
                    {
                        ddlYourRole.SelectedIndex = ddlYourRole.Items.IndexOf(ddlYourRole.Items.FindByValue(oRes.DEMO_BK12_YOUR_ROLE_FK.ToString()));
                    }

                }
            }
            else if (rbtk12OrIHE.SelectedIndex == 1)
            {
                LoadIHEDropdown();
                var oRess = from css in db.IHE_AssessmentQuestions
                            where css.IHE_K12_FK == lngPkID
                            select css;
                foreach (var oRes in oRess)
                {


                    objVal = oRes.DEMO_A_ORGANIZATION_FROM_FK;
                    if (objVal != null)
                    {
                        ddlOrganizationFrom.SelectedIndex = ddlOrganizationFrom.Items.IndexOf(ddlOrganizationFrom.Items.FindByValue(oRes.DEMO_A_ORGANIZATION_FROM_FK.ToString()));
                    }
                    objVal = oRes.DEMO_B_YOUR_ROLE_FK;
                    if (objVal != null)
                    {
                        ddlYourRole.SelectedIndex = ddlYourRole.Items.IndexOf(ddlYourRole.Items.FindByValue(oRes.DEMO_B_YOUR_ROLE_FK.ToString()));
                    }

                }
            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        bool blNewk = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            IHE_K12 oIheK = (from k in db.IHE_K12s where k.K12_IHE_Id == lngPkID select k).FirstOrDefault();

            if (oIheK == null)
            {
                oIheK = new IHE_K12();
                blNewk = true;
            }          
           
            if (!(rbtk12OrIHE.SelectedItem == null))
            {
                if (!(rbtk12OrIHE.SelectedItem.Value.ToString() == ""))
                {
                    oIheK.K12_IHE_Selected = Convert.ToInt32(rbtk12OrIHE.SelectedItem.Value.ToString());
                }
                else
                {
                    oIheK.K12_IHE_Selected = null;
                }
            }

            oIheK.CreatedDate = DateTime.Now;

            if (blNewk == true)
            {
                db.IHE_K12s.InsertOnSubmit(oIheK);

            }

            db.SubmitChanges();
            lngPkID = oIheK.K12_IHE_Id;

         if (!(rbtk12OrIHE.SelectedItem == null))
         {
           
                    if (!(rbtk12OrIHE.SelectedItem.Value.ToString() == "2"))
                    {
                        UpdateK12();
                    }
                    else
                    {
                        UpdateIHE();
                    }
        
           
                }
         }
    }

    private void UpdateIHE()
    {
        bool blNew = false;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            //---------------------------------------
            IHE_AssessmentQuestion oRes = (from c in db.IHE_AssessmentQuestions
                                           where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new IHE_AssessmentQuestion();
                blNew = true;
            }

            if (!(ddlOrganizationFrom.SelectedItem == null))
            {
                if (!(ddlOrganizationFrom.SelectedItem.Value.ToString() == "Select"))
                {
                    oRes.DEMO_A_ORGANIZATION_FROM_FK = Convert.ToInt32(ddlOrganizationFrom.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.DEMO_A_ORGANIZATION_FROM_FK = null;
                }
            }
            if (!(ddlYourRole.SelectedItem == null))
            {
                if (!(ddlYourRole.SelectedItem.Value.ToString() == "Select"))
                {
                    oRes.DEMO_B_YOUR_ROLE_FK = Convert.ToInt32(ddlYourRole.SelectedValue.ToString());
                }
                else
                {
                    oRes.DEMO_B_YOUR_ROLE_FK = null;
                }
            }

            oRes.IHE_K12_FK = lngPkID;
            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.IHE_AssessmentQuestions.InsertOnSubmit(oRes);
            }

            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
    }

private void UpdateK12()
{
        bool blNew = false;  
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            //---------------------------------------
            K12_ASSESSMENT_QUESTION oRes = (from c in db.K12_ASSESSMENT_QUESTIONs
                                            where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new K12_ASSESSMENT_QUESTION();
                blNew = true;
            }

            if (!(ddlOrganizationFrom.SelectedItem == null))
            {
                if (!(ddlOrganizationFrom.SelectedItem.Value.ToString() == "Select"))
                {
                    oRes.DEMO_AK12_ORGANIZATION_FROM_FK = Convert.ToInt32(ddlOrganizationFrom.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.DEMO_AK12_ORGANIZATION_FROM_FK = null;
                }
            }
            if (!(ddlYourRole.SelectedItem == null))
            {
                if (!(ddlYourRole.SelectedItem.Value.ToString() == "Select"))
                {
                    oRes.DEMO_BK12_YOUR_ROLE_FK = Convert.ToInt32(ddlYourRole.SelectedValue.ToString());
                }
                else
                {
                    oRes.DEMO_BK12_YOUR_ROLE_FK = null;
                }
             }

            oRes.IHE_K12_FK = lngPkID;
            oRes.CreatedDate = DateTime.Now;


            if (blNew == true)
            {

                db.K12_ASSESSMENT_QUESTIONs.InsertOnSubmit(oRes);

            }

            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);

        }
}
       
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from p in db.IHEK12_lookups select p;
            rbtk12OrIHE.DataSource = qry;
            rbtk12OrIHE.DataTextField = "IHEK12_Name";
            rbtk12OrIHE.DataValueField = "IHEK12_ID";
            rbtk12OrIHE.DataBind();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (rbtk12OrIHE.SelectedValue == "1")
        {
            updateUser();
            Response.Redirect("B_Introduction.aspx?lngPkID=" + lngPkID);
        }
        else
        {
            updateUser();
            Response.Redirect("B_IHEIntroduction.aspx?lngPkID=" + lngPkID);
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {

        updateUser();
        displayRecords();
        Response.Redirect("Default.aspx?lngPkID=" + lngPkID);

    }
    protected void rbtk12OrIHE_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtk12OrIHE.SelectedValue == "1")
        {
            loadK12Dropdown();            
        }
        else if (rbtk12OrIHE.SelectedValue == "2")
        {
            LoadIHEDropdown();
        }


    }


   protected void  loadK12Dropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
                var qry = from e2 in db.K12_A_ORGANIZATION_FROMs
                          select e2;
                ddlOrganizationFrom.DataSource = qry;
                ddlOrganizationFrom.DataTextField = "K12_A_ORG_NAME";
                ddlOrganizationFrom.DataValueField = "K12_ORG_FROMID";
                ddlOrganizationFrom.DataBind();
                ddlOrganizationFrom.Items.Insert(0, "Select");


                var qry2 = from e1 in db.K12_B_YOUR_ROLEs
                           select e1;

                ddlYourRole.DataSource = qry2;
                ddlYourRole.DataTextField = "K12_YOUR_ROLES";
                ddlYourRole.DataValueField = "K12_YOUR_ROLEID";
                ddlYourRole.DataBind();
                ddlYourRole.Items.Insert(0, "Select");

            }
      }


   protected void LoadIHEDropdown()
   {
       using (DataClasses2DataContext db = new DataClasses2DataContext())
       {
           var qry = from e3 in db.A_DemoOrganizationFroms
                     select e3;
           ddlOrganizationFrom.DataSource = qry;
           ddlOrganizationFrom.DataTextField = "aOrganizationNames";
           ddlOrganizationFrom.DataValueField = "aOrganizationFromId";
           ddlOrganizationFrom.DataBind();
           ddlOrganizationFrom.Items.Insert(0, "Select");


           var qry2 = from e4 in db.B_DemoYourRoles
                      select e4;

           ddlYourRole.DataSource = qry2;
           ddlYourRole.DataTextField = "bYourRoles";
           ddlYourRole.DataValueField = "bYourRoleId";
           ddlYourRole.DataBind();
           ddlYourRole.Items.Insert(0, "Select");



       }
   }
}