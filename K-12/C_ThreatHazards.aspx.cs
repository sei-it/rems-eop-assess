﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class C_ThreatHazards : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "3/10";
    }

    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {

            var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {


                objVal = oRes.THREAT_HAZARDS_AK12_SCHOOL_LIST_OFRISKS_FK;
                if (objVal != null)
                {
                    rbtListOfRiskThreats.SelectedIndex = rbtListOfRiskThreats.Items.IndexOf(rbtListOfRiskThreats.Items.FindByValue(oRes.THREAT_HAZARDS_AK12_SCHOOL_LIST_OFRISKS_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_B1K12_PREVENT_FK;
                if (objVal != null)
                {
                    rbtPrevent.SelectedIndex = rbtPrevent.Items.IndexOf(rbtPrevent.Items.FindByValue(oRes.THREAT_HAZARDS_B1K12_PREVENT_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_B2K12_PROTECT_FK;
                if (objVal != null)
                {
                    rbtProtect.SelectedIndex = rbtProtect.Items.IndexOf(rbtProtect.Items.FindByValue(oRes.THREAT_HAZARDS_B2K12_PROTECT_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_B3K12_MITIGATE_FK;
                if (objVal != null)
                {
                    rbtMitigate.SelectedIndex = rbtMitigate.Items.IndexOf(rbtMitigate.Items.FindByValue(oRes.THREAT_HAZARDS_B3K12_MITIGATE_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_B4K12_RESPOND_FK;
                if (objVal != null)
                {
                    rbtRespond.SelectedIndex = rbtRespond.Items.IndexOf(rbtRespond.Items.FindByValue(oRes.THREAT_HAZARDS_B4K12_RESPOND_FK.ToString()));

                }
                objVal = oRes.THREAT_HAZARDS_B5K12_RECOVER_FK;
                if (objVal != null)
                {
                    rbtRecover.SelectedIndex = rbtRecover.Items.IndexOf(rbtRecover.Items.FindByValue(oRes.THREAT_HAZARDS_B5K12_RECOVER_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_C1K12_LOCKDOWN_FK;
                if (objVal != null)
                {
                    rbtDenyEntry.SelectedIndex = rbtDenyEntry.Items.IndexOf(rbtDenyEntry.Items.FindByValue(oRes.THREAT_HAZARDS_C1K12_LOCKDOWN_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_C2K12_EVACUATION_FK;
                if (objVal != null)
                {
                    rbtEvacuation.SelectedIndex = rbtEvacuation.Items.IndexOf(rbtEvacuation.Items.FindByValue(oRes.THREAT_HAZARDS_C2K12_EVACUATION_FK.ToString()));
                }
                objVal = oRes.THREAT_HAZARDS_C3K12_SHELTER_IN_PLACE_FK;
                if (objVal != null)
                {
                    rbtShelterInPlace.SelectedIndex = rbtShelterInPlace.Items.IndexOf(rbtShelterInPlace.Items.FindByValue(oRes.THREAT_HAZARDS_C3K12_SHELTER_IN_PLACE_FK.ToString()));
                }

            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            K12_ASSESSMENT_QUESTION oRes = (from c in db.K12_ASSESSMENT_QUESTIONs
                                            where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new K12_ASSESSMENT_QUESTION();
                blNew = true;

            }

            if (!(rbtListOfRiskThreats.SelectedItem == null))
            {
                if (!(rbtListOfRiskThreats.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_AK12_SCHOOL_LIST_OFRISKS_FK = Convert.ToInt32(rbtListOfRiskThreats.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_AK12_SCHOOL_LIST_OFRISKS_FK = null;
                }
            }
            if (!(rbtPrevent.SelectedItem == null))
            {
                if (!(rbtPrevent.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_B1K12_PREVENT_FK = Convert.ToInt32(rbtPrevent.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_B1K12_PREVENT_FK = null;
                }
            }
            if (!(rbtProtect.SelectedItem == null))
            {
                if (!(rbtProtect.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_B2K12_PROTECT_FK = Convert.ToInt32(rbtProtect.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_B2K12_PROTECT_FK = null;
                }
            }
            if (!(rbtMitigate.SelectedItem == null))
            {
                if (!(rbtMitigate.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_B3K12_MITIGATE_FK = Convert.ToInt32(rbtMitigate.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_B3K12_MITIGATE_FK = null;
                }
            }
            if (!(rbtRespond.SelectedItem == null))
            {
                if (!(rbtRespond.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_B4K12_RESPOND_FK = Convert.ToInt32(rbtRespond.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_B4K12_RESPOND_FK = null;
                }
            }
            if (!(rbtRecover.SelectedItem == null))
            {
                if (!(rbtRecover.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_B5K12_RECOVER_FK = Convert.ToInt32(rbtRecover.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_B5K12_RECOVER_FK = null;
                }
            }
            if (!(rbtDenyEntry.SelectedItem == null))
            {
                if (!(rbtDenyEntry.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_C1K12_LOCKDOWN_FK = Convert.ToInt32(rbtDenyEntry.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_C1K12_LOCKDOWN_FK = null;
                }
            }
            if (!(rbtEvacuation.SelectedItem == null))
            {
                if (!(rbtEvacuation.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_C2K12_EVACUATION_FK = Convert.ToInt32(rbtEvacuation.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_C2K12_EVACUATION_FK = null;
                }
            }
            if (!(rbtShelterInPlace.SelectedItem == null))
            {
                if (!(rbtShelterInPlace.SelectedItem.Value.ToString() == ""))
                {
                    oRes.THREAT_HAZARDS_C3K12_SHELTER_IN_PLACE_FK = Convert.ToInt32(rbtShelterInPlace.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.THREAT_HAZARDS_C3K12_SHELTER_IN_PLACE_FK = null;
                }
            }


            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.K12_ASSESSMENT_QUESTIONs.InsertOnSubmit(oRes);
            }
            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.K12_YESNO_NOTs
                      select e;


            rbtListOfRiskThreats.DataSource = qry;
            rbtListOfRiskThreats.DataTextField = "Yes_No_Not";
            rbtListOfRiskThreats.DataValueField = "NotID";
            rbtListOfRiskThreats.DataBind();

            rbtPrevent.DataSource = qry;
            rbtPrevent.DataTextField = "Yes_No_Not";
            rbtPrevent.DataValueField = "NotID";
            rbtPrevent.DataBind();

            rbtProtect.DataSource = qry;
            rbtProtect.DataTextField = "Yes_No_Not";
            rbtProtect.DataValueField = "NotID";
            rbtProtect.DataBind();

            rbtMitigate.DataSource = qry;
            rbtMitigate.DataTextField = "Yes_No_Not";
            rbtMitigate.DataValueField = "NotID";
            rbtMitigate.DataBind();

            rbtRespond.DataSource = qry;
            rbtRespond.DataTextField = "Yes_No_Not";
            rbtRespond.DataValueField = "NotID";
            rbtRespond.DataBind();

            rbtRecover.DataSource = qry;
            rbtRecover.DataTextField = "Yes_No_Not";
            rbtRecover.DataValueField = "NotID";
            rbtRecover.DataBind();

            rbtDenyEntry.DataSource = qry;
            rbtDenyEntry.DataTextField = "Yes_No_Not";
            rbtDenyEntry.DataValueField = "NotID";
            rbtDenyEntry.DataBind();

            rbtEvacuation.DataSource = qry;
            rbtEvacuation.DataTextField = "Yes_No_Not";
            rbtEvacuation.DataValueField = "NotID";
            rbtEvacuation.DataBind();

            rbtShelterInPlace.DataSource = qry;
            rbtShelterInPlace.DataTextField = "Yes_No_Not";
            rbtShelterInPlace.DataValueField = "NotID";
            rbtShelterInPlace.DataBind();
        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("D_AccountingCommunication.aspx?lngPkID=" + lngPkID);
    }




    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("B_Introduction.aspx?lngPkID=" + lngPkID);
    }
}