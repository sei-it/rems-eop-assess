﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="B_IHEIntroduction.aspx.cs" Inherits="B_IHEIntroduction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <h1>Collaborative Planning</h1>
<p >Whether or not you currently have a plan, you are in the right place! This assessment will help you recognize areas of higher education safety planning in which you may already be engaged, and those in which you can expand or enhance, including recommendations for how to move forward on both.</p>
<fieldset class="assessForm">
               <legend for="rbtEmergencyPlan">1. Does your IHE currently have an emergency plan?
                 <asp:RequiredFieldValidator ID="reqYourRole" runat="server" 
                  ValidationGroup="Save" ControlToValidate="rbtEmergencyPlan"
                 ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator></legend>
              
               <asp:RadioButtonList ID="rbtEmergencyPlan" CssClass="leftAlign" runat="server"  RepeatLayout="Flow">
               </asp:RadioButtonList>  

     <label  class="noBorder1" for="rbtEmergencyPlan" > An emergency plan describes who will do what, as well as when, with what resources, and by what authority--before, during, and immediately after an emergency.</label>

 
</fieldset>
<fieldset class="assessForm">
             <legend for="rbtIndividualTeamSafety">2. Does your IHE have an individual or team in charge of safety? 
                    <asp:RequiredFieldValidator ID="reqIndividualTeamSafety" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtIndividualTeamSafety"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator> </legend>        
                
            <asp:RadioButtonList ID="rbtIndividualTeamSafety" CssClass="leftAlign"  runat="server"   RepeatLayout="Flow">
            </asp:RadioButtonList>  
    
     <label  class="noBorder1" for="rbtEmergencyPlan" >A high-quality emergency operations plan is created through a process that involves the contributions of many individuals and organizations who have a responsibility in keeping your campus and its community safe, such as Campus Police, Administrators, Local Law Enforcement, Faculty, Environmental Health and Safety, and many others.</label>

               
</fieldset>
    
 <fieldset class="assessForm multiPart">

     <legend>
     	3. Have you considered reaching out to the following community partners to discuss collaborating on response strategies to an emergency event?
     </legend>       
           
            <div style="position: relative; display: block; width: 100%;clear:both; top: 52px; left: 0px;">ALSFJSLMFWEFLWMSDFLDSMFLEWM</div>
      <div class="questionGroup">
           <label for="rbtFirstResponders">First responders?
                <asp:RequiredFieldValidator ID="reqFirstResponders" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtFirstResponders"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtFirstResponders" runat="server" CssClass="leftAlign"  RepeatLayout="Flow" >

            </asp:RadioButtonList>
          	</div><!-- /questionGroup -->
    
       <div class="questionGroup"> 
            <label for="rbtLocalEmergency">Local emergency managers?
                <asp:RequiredFieldValidator ID="reqLocalEmergency" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtLocalEmergency"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

            </label>
            <asp:RadioButtonList ID="rbtLocalEmergency" runat="server" CssClass="leftAlign"  RepeatLayout="Flow" >

            </asp:RadioButtonList>
      	</div><!-- /questionGroup -->
      <div class="questionGroup"> 
            <label for="rblLawEnforcement">Local law enforcement officers?
                 <asp:RequiredFieldValidator ID="reqLawEnforcement" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rblLawEnforcement"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rblLawEnforcement" CssClass="leftAlign" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>

     	</div><!-- /questionGroup -->

     <div class="questionGroup"> 
            <label for="rbtEMSpersonnel">Emergency medical services (EMS) personnel?

                 <asp:RequiredFieldValidator ID="reqEMSpersonnel" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtEMSpersonnel"
                  ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
            </label>
            <asp:RadioButtonList ID="rbtEMSpersonnel" CssClass="leftAlign" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>
      	</div><!-- /questionGroup -->

  <div class="questionGroup"> 
            <label for="rbtFireOfficials">Fire officials? 
                 <asp:RequiredFieldValidator ID="reqFireOfficials" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtFireOfficials"
                     ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

            </label>
            <asp:RadioButtonList ID="rbtFireOfficials" CssClass="leftAlign" runat="server"  RepeatLayout="Flow" >

            </asp:RadioButtonList>

         	</div><!-- /questionGroup -->
        <div class="questionGroup">
            <label for="rbtHealthPractitioners">Public and mental health practitioners?

                  <asp:RequiredFieldValidator ID="reqHealthPractitioners" runat="server" 
                     ValidationGroup="Save" ControlToValidate="rbtHealthPractitioners"
                    ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>

            </label>
            <asp:RadioButtonList ID="rbtHealthPractitioners" runat="server" CssClass="leftAlign"  RepeatLayout="Flow" >

            </asp:RadioButtonList>
           </div><!-- /questionGroup -->

                  <label class="noBorder1" for="rbtHealthPractitioners"><i>Not only should these partners be a part of the planning process, but the final plan should be presented to the appropriate leadership for official approval, and shared with community partners who have a responsibility in carrying out the plan. Your IHE should also consider measures to protect the plan from those who are not authorized to have it.</i></label> 
          
          
            
         </fieldset>
    <div>
          <div class="prevNext">
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE"  ValidationGroup="Save" runat="server" Text="Next" OnClick="btnNextIHE_Click" />
        
    	</div>
         
          <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red"  CssClass="ValidationSummary"
            HeaderText="All required information was not provided. Please answer the marked question(s)." />
         <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    </div>



            <div id='PopUpE' class="PopUp" ><span class="b-close"></span>
             <span> An emergency plan describes who will
              do what, as well as when, with what resources, and by what 
              authority--before, during, and immediately after an emergency.
             </span>        
            </div>
            <div id='PopUp' class="PopUp" ><span class="b-close"></span>
              <span>Whether or not you currently have a plan, you are in the right place! 
                  This assessment will help you recognize areas of higher education safety planning in which you
                   may already be engaged, and those in which you can expand or enhance, including recommendations 
                  for how to move forward on both.</span>
             </div>

            <div id='PopUp2' class="PopUp" ><span class="b-close"></span>
              <span>A high-quality emergency operations plan is created through a process that involves 
                  the contributions of many individuals and organizations who have a responsibility in 
                  keeping your campus and its community safe. Consider the following recommended roles 
                  for participation on your planning team.</span>
            </div>

            <div id='PopUp3' class="PopUp"><span class="b-close"></span>
              <span>Not only should these partners be a part of the planning process, but the final 
                  plan should be presented to the appropriate leadership for official approval, 
                  and shared with community partners who have a responsibility in carrying out the 
                  plan. Your IHE should also consider measures to protect the plan from those 
                  who are not authorized to have it.</span>
             </div>
    
</asp:Content>

