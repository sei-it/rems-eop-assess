﻿<%@ Page Title="" Language="C#" MasterPageFile="remsMasterPage.master" AutoEventWireup="true" CodeFile="F_IHELawsRegulations.aspx.cs" Inherits="F_IHELawsRegulations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="Scripts/bPopup.min.js"></script>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" runat="Server">
    <h1>Laws and Regulations</h1>

    <fieldset class="assessForm">
        <legend>12. Are you aware that there are local, state, and Federal laws
       that relate to campus safety and that impact how you and your IHE should plan for emergencies?

        <asp:RequiredFieldValidator ID="reqLocalStateFederalLaw" runat="server"
            ValidationGroup="Save" ControlToValidate="rbtLocalStateFederalLaw"
            ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
</legend>
       
        <asp:RadioButtonList ID="rbtLocalStateFederalLaw" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

    </fieldset>
    <fieldset class="assessForm">
        <legend>13. If you already have an emergency plan, 
      are you aware whether or not the plan already incorporates all applicable 
      <a class="MouesoverPopup">laws and regulations? </a>
            <label></label>
       
        <asp:RequiredFieldValidator ID="reqAlreadyHaveEmgPlan" runat="server"
            ValidationGroup="Save" ControlToValidate="rbtAlreadyHaveEmgPlan"
            ForeColor="Red" Font-Size="Medium">Required</asp:RequiredFieldValidator>
 </legend>
        <label for="rbtAlreadyHaveEmgPlan"></label>
        <asp:RadioButtonList ID="rbtAlreadyHaveEmgPlan" runat="server" CssClass="leftAlign" RepeatDirection="Vertical" RepeatLayout="Flow">
        </asp:RadioButtonList>

        <label class="noBorder1" for="rbtAddressNeedsWithDisab"><i>Information on applicable laws and regulations may be located on local or state government webpages. Additional information on federal statutes that have implications for information sharing in the emergency planning process may be found in the IHE Key Topics section of the REMS TA Center website.</i></label>

    </fieldset>



    <div class="prevNext">
        <asp:Button ID="btnBackIHE" runat="server" Text="Go Back" OnClick="btnBackIHE_Click" />
        <asp:Button ID="btnNextIHE" runat="server" Text="Next" ValidationGroup="Save" OnClick="btnNextIHE_Click" />

    </div>


    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" CssClass="ValidationSummary"
        HeaderText="All required information was not provided. Please answer the marked question(s)." />
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>


    <div id='PopUp' class="PopUp">
        <span class="b-close"></span>
        <span>Information on applicable laws and regulations may be located on local or 
                  state government webpages. Additional information on federal statutes that have 
                  implications for information sharing in the emergency planning process may be 
                  found in the IHE Resources section of the REMS TA Center website.</span>
    </div>

</asp:Content>


