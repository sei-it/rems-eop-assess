﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class H_Recovery : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            loadDropdown();
            displayRecords();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Session["Pagetext"] = "8/10";
    }

    protected void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oRess = from css in db.K12_ASSESSMENT_QUESTIONs
                        where css.IHE_K12_FK == lngPkID
                        select css;
            foreach (var oRes in oRess)
            {
                objVal = oRes.RECOVERY_A1K12_CON_BUSINESS_OPERATIONS_FK;
                if (objVal != null)
                {
                    rbtContinuityBussOperations.SelectedIndex = rbtContinuityBussOperations.Items.IndexOf(rbtContinuityBussOperations.Items.FindByValue(oRes.RECOVERY_A1K12_CON_BUSINESS_OPERATIONS_FK.ToString()));
                }
                objVal = oRes.RECOVERY_A2K12_CON_TEACHING_LEARNING_FK;
                if (objVal != null)
                {
                    rbtContinuityTeaching.SelectedIndex = rbtContinuityTeaching.Items.IndexOf(rbtContinuityTeaching.Items.FindByValue(oRes.RECOVERY_A2K12_CON_TEACHING_LEARNING_FK.ToString()));
                }
                objVal = oRes.RECOVERY_A3K12_HELP_STUDENTS_STAFF_FK;
                if (objVal != null)
                {
                    rbtHelpingStudents.SelectedIndex = rbtHelpingStudents.Items.IndexOf(rbtHelpingStudents.Items.FindByValue(oRes.RECOVERY_A3K12_HELP_STUDENTS_STAFF_FK.ToString()));
                }
                objVal = oRes.RECOVERY_BK12_SCHOOL_CONSIDERED_STRATEGIES_FK;
                if (objVal != null)
                {
                    rbtIHEconsiderStrategies.SelectedIndex = rbtIHEconsiderStrategies.Items.IndexOf(rbtIHEconsiderStrategies.Items.FindByValue(oRes.RECOVERY_BK12_SCHOOL_CONSIDERED_STRATEGIES_FK.ToString()));
                }
            }
        }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            K12_ASSESSMENT_QUESTION oRes = (from c in db.K12_ASSESSMENT_QUESTIONs
                                            where c.IHE_K12_FK == lngPkID
                                            select c).FirstOrDefault();
            if ((oRes == null))
            {
                oRes = new K12_ASSESSMENT_QUESTION();
                blNew = true;

            }

            if (!(rbtContinuityBussOperations.SelectedItem == null))
            {
                if (!(rbtContinuityBussOperations.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_A1K12_CON_BUSINESS_OPERATIONS_FK = Convert.ToInt32(rbtContinuityBussOperations.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_A1K12_CON_BUSINESS_OPERATIONS_FK = null;
                }
            }
            if (!(rbtContinuityTeaching.SelectedItem == null))
            {
                if (!(rbtContinuityTeaching.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_A2K12_CON_TEACHING_LEARNING_FK = Convert.ToInt32(rbtContinuityTeaching.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_A2K12_CON_TEACHING_LEARNING_FK = null;
                }
            }

            if (!(rbtHelpingStudents.SelectedItem == null))
            {
                if (!(rbtHelpingStudents.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_A3K12_HELP_STUDENTS_STAFF_FK = Convert.ToInt32(rbtHelpingStudents.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_A3K12_HELP_STUDENTS_STAFF_FK = null;
                }
            }
            if (!(rbtIHEconsiderStrategies.SelectedItem == null))
            {
                if (!(rbtIHEconsiderStrategies.SelectedItem.Value.ToString() == ""))
                {
                    oRes.RECOVERY_BK12_SCHOOL_CONSIDERED_STRATEGIES_FK = Convert.ToInt32(rbtIHEconsiderStrategies.SelectedItem.Value.ToString());
                }
                else
                {
                    oRes.RECOVERY_BK12_SCHOOL_CONSIDERED_STRATEGIES_FK = null;
                }
            }

            oRes.CreatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.K12_ASSESSMENT_QUESTIONs.InsertOnSubmit(oRes);
            }
            db.SubmitChanges();
            lngPkID = Convert.ToInt32(oRes.IHE_K12_FK);
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


    }
    protected void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.K12_YESNO_NOTs
                      select e;

            rbtContinuityBussOperations.DataSource = qry;
            rbtContinuityBussOperations.DataTextField = "Yes_No_Not";
            rbtContinuityBussOperations.DataValueField = "NotID";
            rbtContinuityBussOperations.DataBind();

            rbtContinuityTeaching.DataSource = qry;
            rbtContinuityTeaching.DataTextField = "Yes_No_Not";
            rbtContinuityTeaching.DataValueField = "NotID";
            rbtContinuityTeaching.DataBind();


            rbtHelpingStudents.DataSource = qry;
            rbtHelpingStudents.DataTextField = "Yes_No_Not";
            rbtHelpingStudents.DataValueField = "NotID";
            rbtHelpingStudents.DataBind();


            rbtIHEconsiderStrategies.DataSource = qry;
            rbtIHEconsiderStrategies.DataTextField = "Yes_No_Not";
            rbtIHEconsiderStrategies.DataValueField = "NotID";
            rbtIHEconsiderStrategies.DataBind();


        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("I_OtherConsideration.aspx?lngPkID=" + lngPkID);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("G_TrainingDocumentation.aspx?lngPkID=" + lngPkID);
    }
}